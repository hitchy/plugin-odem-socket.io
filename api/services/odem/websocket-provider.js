"use strict";

const { promisify } = require( "node:util" );

module.exports = function() {
	const api = this;

	const logDebug = api.log( "hitchy:plugin:odem:socket.io:debug" );
	const logInfo = api.log( "hitchy:plugin:odem:socket.io:info" );
	const logError = api.log( "hitchy:plugin:odem:socket.io:error" );

	/**
	 * Implements API for controlling and monitoring server-side
	 * document-oriented database via websocket.
	 */
	class OdemWebsocketProvider {
		static #socketUsers = new Map();

		/**
		 * Integrates this provider with server-side websocket.
		 *
		 * @returns {void}
		 */
		static start() {
			api.once( "websocket", io => {
				const { crud, notifications } = api.config.socket.odem;
				const namespace = io.of( "/hitchy/odem" );


				// - transmit notifications to clients ------------------------

				const txPerModel = {};

				for ( const [ modelName, model ] of Object.entries( api.models ) ) {
					if ( api.service.OdemSchema.mayBeExposed( { user: undefined }, model ) ) {
						txPerModel[modelName] = this.broadcastModelNotifications( notifications, namespace, model );
					}
				}

				api.once( "close", () => {
					namespace.disconnectSockets( true );

					for ( const [ modelName, handlers ] of Object.entries( txPerModel ) ) {
						if ( handlers ) {
							const model = api.models[modelName];

							model.notifications
								.off( "created", handlers.created )
								.off( "changed", handlers.changed )
								.off( "removed", handlers.removed );
						}
					}
				} );


				// - receive requests from clients ----------------------------

				namespace.on( "connection", socket => {
					this.getUserForSocket( socket );

					const listener = ( query, ...args ) => this.handleControlRequest( crud, socket, query, args );

					socket.onAny( listener );

					socket.once( "disconnect", () => {
						socket.offAny( listener );

						this.#socketUsers.delete( socket.id );
					} );
				} );
			} );
		}

		/**
		 * Monitors models of document-oriented database and sends notifications
		 * to connected clients in case items of either model have changed.
		 *
		 * @param {boolean} enabled true if command handling is enabled in configuration
		 * @param {Server} namespace namespace managing a list of server-side sockets with common prefix
		 * @param {class<Hitchy.Plugin.Odem.Model>} Model model to monitor for change of items
		 * @returns {Object<string,function>|undefined} map of registered handler functions
		 */
		static broadcastModelNotifications( enabled, namespace, Model ) {
			if ( enabled ) {
				const handlers = {
					created: ( uuid, newRecord, asyncGeneratorFn ) => {
						asyncGeneratorFn()
							.then( item => this.namespaceEmit( namespace, Model, {
								change: "created",
								model: Model.name,
								properties: item.toObject( { serialized: true } ),
							}, true ) )
							.catch( cause => {
								logError( "broadcasting notification on having created item %s of %s has failed:", uuid, Model.name, cause.stack );
							} );
					},
					changed: ( uuid, newRecord, oldRecord, asyncGeneratorFn ) => {
						asyncGeneratorFn()
							.then( item => this.namespaceEmit( namespace, Model, {
								change: "updated",
								model: Model.name,
								properties: item.toObject( { serialized: true } ),
							}, true ) )
							.catch( cause => {
								logError( "broadcasting notification on having updated item %s of %s has failed:", uuid, Model.name, cause.stack );
							} );
					},
					removed: uuid => {
						this.namespaceEmit( namespace, Model, {
							change: "deleted",
							model: Model.name,
							properties: { uuid: Model.formatUUID( uuid ) },
						}, false )
							.catch( cause => {
								logError( "broadcasting notification on having removed item %s of %s has failed:", uuid, Model.name, cause.stack );
							} );
					},
				};

				Model.notifications
					.on( "created", handlers.created )
					.on( "changed", handlers.changed )
					.on( "removed", handlers.removed );

				return handlers;
			}

			return undefined;
		}

		/**
		 * Reads promise for given socket's requesting user from local cache.
		 *
		 * This method is mocking Hitchy's request dispatching to invoke
		 * policies of @hitchy/plugin-auth involved in setting up a requesting
		 * user to be returned here eventually. The mocked request is based on
		 * the provided socket's initial request.
		 *
		 * @param {WebSocket} socket socket to look up
		 * @returns {Promise<User|undefined>} promise for user of provided socket
		 */
		static async getUserForSocket( socket ) {
			if ( !this.#socketUsers.has( socket.id ) ) {
				this.#socketUsers.set( socket.id, ( async _socket => {
					if ( api.plugins.authentication ) {
						const { SessionInjector, Cookies } = api.service;
						const { Authentication } = api.policy;
						const useSession = api.plugins.session && !api.config?.session?.disable;
						const headers = _socket.request?.headers;

						// mock context used by Hitchy for dispatching requests
						const request = {
							headers,
							cookies: Cookies.parseFromString( headers?.cookie ),
						};

						const response = {
							set: () => {}, // eslint-disable-line no-empty-function
						};

						const context = {
							local: {},
							updateSessionId: ( _, __, fn ) => fn(),
						};

						// invoke policies usually injecting a requesting user into a request
						if ( useSession ) {
							try {
								await SessionInjector.injectIntoRequest( request, response );
								await promisify( Authentication.injectPassport.bind( context ) )( request, response );
							} catch ( cause ) {
								logError( "mocking session-handling and passport injection has failed:", cause );
							}
						}

						if ( !request.user ) {
							try {
								await promisify( Authentication.handleBasicAuth.bind( context ) )( request, response );
							} catch ( cause ) {
								logError( "mocking Basic authentication handling has failed:", cause );
							}
						}

						// pick user eventually injected into mocked request
						return request.user || undefined;
					}

					return undefined;
				} )( socket ) );
			}

			return await this.#socketUsers.get( socket.id );
		}

		/**
		 * Broadcasts a change notification to every connected peer in a given
		 * namespace individually considering either peer's authorization for
		 * accessing the changed model and/or its properties.
		 *
		 * @param {Server} namespace namespace managing a pool of sockets that have joined the namespace
		 * @param {class<Hitchy.Plugin.Odem.Model>} Model model of changed item
		 * @param {{change: string, model: string, properties: Object}} payload raw payload of model-specific notification
		 * @param {boolean} filterProperties if true, properties need to be filtered per peer based on its authorization
		 * @returns {Promise<void>} promise resolved when all authorized peers have been notified
		 */
		static async namespaceEmit( namespace, Model, payload, filterProperties ) {
			const hasPluginAuth = Boolean( api.plugins.authentication );
			const sockets = await namespace.fetchSockets();

			logDebug( "forwarding notification to %d socket(s)", sockets.length );

			const notifyPeer = async socket => {
				const { OdemSchema, Authorization } = api.service;
				let user;

				try {
					user = await this.#socketUsers.get( socket.id );
				} catch ( cause ) {
					logError( `discovering user of socket ${socket.id} has failed:`, cause );
					return;
				}

				if ( !OdemSchema.mayBeExposed( { user }, Model ) ) {
					logDebug( `omit notification to user ${user?.uuid} (${user.name}) at peer ${socket.id} with access on ${Model.name} forbidden by model` );
					return;
				}

				let mayRead = OdemSchema.isAdmin( user );
				let scope = "read";

				if ( !mayRead && hasPluginAuth ) {
					mayRead = await Authorization.mayAccess( user, `@hitchy.odem.model.${Model.name}.read` );

					if ( !mayRead ) {
						mayRead = await Authorization.mayAccess( user, `@hitchy.odem.model.${Model.name}.list` );
						scope = "list";
					}
				}

				const prefix = api.utility.case.pascalToKebab( Model.name );

				if ( !mayRead ) {
					// user must not read items of this model -> do not provide any identifier, but basically notify on some change to the model
					socket.emit( `${prefix}:changed`, { ...payload, properties: {} } );
					socket.emit( "*:changed", { ...payload, properties: {} } );
				} else if ( filterProperties ) {
					const copy = { ...payload };

					copy.properties = OdemSchema.filterItem( copy.properties, { user }, Model, scope );

					socket.emit( `${prefix}:changed`, copy );
					socket.emit( "*:changed", copy );
				} else {
					socket.emit( `${prefix}:changed`, payload );
					socket.emit( "*:changed", payload );
				}
			};

			for ( const socket of sockets ) {
				notifyPeer( socket ).catch( cause => {
					logError(
						`notifying peer ${socket.id} on having ${payload.change} item of ${Model.name} has failed:`,
						cause.stack
					);
				} );
			}
		}

		/**
		 * Commonly handles incoming messages to process those requesting
		 * supported control actions on existing models.
		 *
		 * @param {boolean} enabled if true, this plugin's control API has been enabled in configuration
		 * @param {WebSocket} socket request-receiving socket
		 * @param {string} query name of event that has been emitted by peer socket to request some action
		 * @param {any[]} args arguments of incoming request and a callback for sending a response in final argument
		 * @returns {void}
		 */
		static handleControlRequest( enabled, socket, query, args ) {
			const match = /^([a-zA-Z][a-zA-Z0-9]+):(list|find|create|read|write|update|remove|delete)$/.exec( query );

			if ( !match ) {
				logDebug( "ignoring socket-based request %s due to mismatching format", query );
				return;
			}

			const [ , prefix, action ] = match;
			const modelName = api.utility.case.kebabToPascal( prefix );
			const Model = api.model[modelName];

			if ( !Model ) {
				logDebug( "rejecting socket-based request %s due to addressing unknown model %s", query, modelName );

				args[args.length - 1]( {
					error: "no such model " + modelName,
				} );
				return;
			}

			const handler = {
				list: "handleListRequest",
				find: "handleFindRequest",
				create: "handleCreateRequest",
				read: "handleReadRequest",
				write: "handleUpdateRequest",
				update: "handleUpdateRequest",
				remove: "handleDeleteRequest",
				delete: "handleDeleteRequest",
			}[action];

			if ( !handler ) {
				logDebug( "ignoring socket-based request for actin %s on model %s due to lack of handler", action, Model.name );
				return;
			}

			if ( enabled ) {
				this[handler]( socket, Model, ...args );
			} else {
				logDebug( "rejecting socket-based request for action %s on model %s", action, Model.name );

				reject( args[args.length - 1] );
			}
		}

		/**
		 * Handles incoming request for listing items of a model.
		 *
		 * @param {WebSocket} socket receiving socket of request
		 * @param {class<Hitchy.Plugin.Odem.Model>} Model named server-side model
		 * @param {Hitchy.Odem.ListOptions} queryOptions client-provided options for listing items
		 * @param {boolean} loadRecords if true, whole records shall be fetched per item
		 * @param {function} response callback to invoke with response
		 * @returns {Promise<void>} promise settled on request handled
		 */
		static async handleListRequest( socket, Model, queryOptions, loadRecords, response ) {
			logDebug( "request for listing %s instances w/ queryOptions %j and loadRecords %j", Model.name, queryOptions, Boolean( loadRecords ) );

			try {
				const user = await this.getUserForSocket( socket );

				if ( api.plugins.authentication ) {
					if ( !await api.service.Authorization.mayAccess( user, `@hitchy.odem.model.${Model.name}.list` ) ) {
						throw new Error( "access forbidden" );
					}
				}

				if ( !api.service.OdemSchema.mayBeExposed( { user }, Model ) ) {
					throw new Error( "access forbidden by model" );
				}

				const meta = {};
				const items = await Model.list( queryOptions, { loadRecords: Boolean( loadRecords ), metaCollector: meta } );
				let filter;

				if ( loadRecords ) {
					filter = item => api.service.OdemSchema.filterItem( item.toObject( { serialized: true } ), { user }, Model, "list" );
				} else {
					filter = item => ( { uuid: item.uuid } );
				}

				response( {
					success: true,
					count: meta.count,
					items: items.map( filter ),
				} );
			} catch ( error ) {
				logError( "listing %s instances failed: %s", Model.name, error.stack );

				response( {
					error: error.message,
				} );
			}
		}

		/**
		 * Handles incoming request for finding items of a model matching some
		 * provided query.
		 *
		 * @param {WebSocket} socket receiving socket of request
		 * @param {class<Hitchy.Plugin.Odem.Model>} Model named server-side model
		 * @param {Hitchy.Odem.Query} query client-provided constraints to be met by returned items
		 * @param {Hitchy.Odem.ListOptions} queryOptions client-provided options for listing items
		 * @param {boolean} loadRecords if true, whole records shall be fetched per item
		 * @param {function} response callback to invoke with response
		 * @returns {Promise<void>} promise settled on request handled
		 */
		static async handleFindRequest( socket, Model, query, queryOptions, loadRecords, response ) {
			logDebug( "request for finding %s instances matching %j w/ queryOptions %j and uuidsOnly %j", Model.name, query, queryOptions, Boolean( loadRecords ) );

			try {
				const user = await this.getUserForSocket( socket );

				if ( api.plugins.authentication ) {
					if ( !await api.service.Authorization.mayAccess( user, `@hitchy.odem.model.${Model.name}.list` ) ) {
						throw new Error( "access forbidden" );
					}
				}

				if ( !api.service.OdemSchema.mayBeExposed( { user }, Model ) ) {
					throw new Error( "access forbidden by model" );
				}

				const meta = {};
				const items = await Model.find( query, queryOptions, { loadRecords: Boolean( loadRecords ), metaCollector: meta } );
				let filter;

				if ( loadRecords ) {
					filter = item => api.service.OdemSchema.filterItem( item.toObject( { serialized: true } ), { user }, Model, "list" );
				} else {
					filter = item => ( { uuid: item.uuid } );
				}

				response( {
					success: true,
					count: meta.count,
					items: items.map( filter ),
				} );
			} catch ( error ) {
				logError( "finding %s instances matching %j failed: %s", Model.name, query, error.stack );

				response( {
					error: error.message,
				} );
			}
		}

		/**
		 * Handles incoming request for creating item of a model.
		 *
		 * @param {WebSocket} socket receiving socket of request
		 * @param {class<Hitchy.Plugin.Odem.Model>} Model named server-side model
		 * @param {Hitchy.Odem.ListOptions} properties properties of item to create
		 * @param {function} response callback to invoke with response
		 * @returns {Promise<void>} promise settled on request handled
		 */
		static async handleCreateRequest( socket, Model, properties, response ) {
			logDebug( "request for creating %s instance with %j", Model.name, properties );

			const { Authorization, OdemSchema } = api.service;

			try {
				const user = await this.getUserForSocket( socket );

				if ( api.plugins.authentication && !await Authorization.mayAccess( user, `@hitchy.odem.model.${Model.name}.create` ) ) {
					throw new Error( "access forbidden" );
				}

				if ( !OdemSchema.mayBeExposed( { user }, Model ) ) {
					throw new Error( "access forbidden by model" );
				}

				const schema = Model.schema;
				const filtered = OdemSchema.filterItem( properties, { user }, Model, "create" );
				const instance = new Model();

				for ( const [ propName, propValue ] of Object.entries( filtered ) ) {
					if ( schema.props.hasOwnProperty( propName ) || schema.computed.hasOwnProperty( propName ) ) {
						instance[propName] = propValue;
					}
				}

				await instance.save();

				response( {
					success: true,
					properties: OdemSchema.filterItem( instance.toObject( { serialized: true } ), { user }, Model, "read" ),
				} );
			} catch ( error ) {
				logError( "creating %s instance failed: %s", Model.name, error.stack );

				response( {
					error: error.message,
				} );
			}
		}

		/**
		 * Handles incoming request for reading properties of a single item.
		 *
		 * @param {WebSocket} socket receiving socket of request
		 * @param {class<Hitchy.Plugin.Odem.Model>} Model named server-side model
		 * @param {string} uuid client-provided UUID of item to read
		 * @param {function} response callback to invoke with response
		 * @returns {Promise<void>} promise settled on request handled
		 */
		static async handleReadRequest( socket, Model, uuid, response ) {
			logDebug( "request for reading %s instance %s", Model.name, uuid );

			try {
				const user = await this.getUserForSocket( socket );

				if ( api.plugins.authentication ) {
					if ( !await api.service.Authorization.mayAccess( user, `@hitchy.odem.model.${Model.name}.read` ) ) {
						throw new Error( "access forbidden" );
					}
				}

				if ( !api.service.OdemSchema.mayBeExposed( { user }, Model ) ) {
					throw new Error( "access forbidden by model" );
				}

				const instance = new Model( uuid );
				await instance.load();

				response( {
					success: true,
					properties: api.service.OdemSchema.filterItem( instance.toObject( { serialized: true } ), { user }, Model, "read" ),
				} );
			} catch ( error ) {
				logError( "reading %s instance failed: %s", Model.name, error.stack );

				response( {
					error: error.message,
				} );
			}
		}

		/**
		 * Handles incoming request for adjusting properties of a single item.
		 *
		 * @param {WebSocket} socket receiving socket of request
		 * @param {class<Hitchy.Plugin.Odem.Model>} Model named server-side model
		 * @param {string} uuid client-provided UUID of item to modify
		 * @param {Object} properties client-provided properties of item replacing its existing ones
		 * @param {function} response callback to invoke with response
		 * @returns {Promise<void>} promise settled on request handled
		 */
		static async handleUpdateRequest( socket, Model, uuid, properties, response ) {
			logDebug( "request for updating %s instance %s with %j", Model.name, uuid, properties );

			try {
				const user = await this.getUserForSocket( socket );

				if ( api.plugins.authentication ) {
					if ( !await api.service.Authorization.mayAccess( user, `@hitchy.odem.model.${Model.name}.write` ) ) {
						throw new Error( "access forbidden" );
					}
				}

				if ( !api.service.OdemSchema.mayBeExposed( { user }, Model ) ) {
					throw new Error( "access forbidden by model" );
				}

				const schema = Model.schema;
				const filtered = api.service.OdemSchema.filterItem( properties, { user }, Model, "write" );
				const instance = new Model( uuid );
				await instance.load();

				for ( const [ propName, propValue ] of Object.entries( filtered ) ) {
					if ( schema.props.hasOwnProperty( propName ) || schema.computed.hasOwnProperty( propName ) ) {
						instance[propName] = propValue;
					}
				}

				await instance.save();

				response( {
					success: true,
					properties: api.service.OdemSchema.filterItem( instance.toObject( { serialized: true } ), { user }, Model, "read" ),
				} );
			} catch ( error ) {
				logError( "updating %s instance failed: %s", Model.name, error.stack );

				response( {
					error: error.message,
				} );
			}
		}

		/**
		 * Handles incoming request for removing a model's item.
		 *
		 * @param {WebSocket} socket receiving socket of request
		 * @param {class<Hitchy.Plugin.Odem.Model>} Model named server-side model
		 * @param {string} uuid client-provided UUID of item to remove
		 * @param {function} response callback to invoke with response
		 * @returns {Promise<void>} promise settled on request handled
		 */
		static async handleDeleteRequest( socket, Model, uuid, response ) {
			logDebug( "request for deleting %s instance %s", Model.name, uuid );

			try {
				const user = await this.getUserForSocket( socket );

				if ( api.plugins.authentication ) {
					if ( !await api.service.Authorization.mayAccess( user, `@hitchy.odem.model.${Model.name}.remove` ) ) {
						throw new Error( "access forbidden" );
					}
				}

				if ( !api.service.OdemSchema.mayBeExposed( { user }, Model ) ) {
					throw new Error( "access forbidden by model" );
				}

				const instance = new Model( uuid );
				await instance.remove();

				response( {
					success: true,
					properties: { uuid },
				} );
			} catch ( error ) {
				logError( "deleting %s instance %s failed: %s", Model.name, uuid, error.stack );

				response( {
					error: error.message,
				} );
			}
		}
	}

	return OdemWebsocketProvider;
};

/**
 * Commonly responds to action requests in case of having disabled them.
 *
 * @param {function(response: any):void} respondFn callback to be invoked for responding to a peer event
 * @returns {void}
 */
function reject( respondFn ) {
	respondFn( {
		error: "requested action is not available due to runtime configuration",
	} );
}
