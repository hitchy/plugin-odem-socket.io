"use strict";

const { describe, it, beforeEach, afterEach } = require( "mocha" );
const SDT = require( "@hitchy/server-dev-tools" );
const { io } = require( "socket.io-client" );

require( "should" );


describe( "Notification on changes made to document-oriented database", () => {
	describe( "with API for controlling data being disabled", () => {
		const ctx = {};

		afterEach( SDT.after( ctx ) );
		beforeEach( SDT.before( ctx, {
			projectFolder: false,
			plugin: true,
			files: {
				"config/auth.js": `exports.auth = { authorizations: { "@hitchy.odem": "*" } }`,
				"api/model/todo.js": `module.exports = { props: { title: {}, prio: { type: "number" } } }`,
			}
		} ) );

		beforeEach( () => {
			ctx.url = `http://127.0.0.1:${ctx.server.address().port}/hitchy/odem`;
		} );

		it( "emits broadcast on server-side creation of an instance of a server-side model", async() => {
			let original;

			const result = await new Promise( ( resolve, reject ) => {
				const socket = io( ctx.url );

				socket.once( "connect_error", reject );
				socket.once( "todo:changed", record => {
					socket.close();
					resolve( record );
				} );

				socket.once( "connect", () => {
					original = new ctx.hitchy.api.models.Todo();
					original.title = "another title";
					original.prio = 123;
					original.save();
				} );
			} );

			result.should.be.Object().which.has.size( 3 ).and.has.properties( "change", "properties", "model" );
			result.change.should.be.String().which.is.equal( "created" );
			result.properties.should.be.Object().which.has.size( 3 ).and.has.properties( "uuid", "title", "prio" );
			result.properties.uuid.should.be.equal( original.uuid );
			result.properties.title.should.be.equal( "another title" );
			result.properties.prio.should.be.equal( 123 );
			result.model.should.be.equal( "Todo" );
		} );

		it( "emits broadcast on server-side update of an instance of a server-side model", async() => {
			const original = new ctx.hitchy.api.models.Todo();
			original.title = "another title";
			original.prio = 123;
			await original.save();

			const result = await new Promise( ( resolve, reject ) => {
				const socket = io( ctx.url );

				socket.once( "connect_error", reject );
				socket.once( "todo:changed", record => {
					socket.close();
					resolve( record );
				} );

				socket.once( "connect", async() => {
					const update = new ctx.hitchy.api.models.Todo( original.uuid );
					await update.load();

					update.title = "adjusted title";
					update.prio = "-321";
					update.save();
				} );
			} );

			result.should.be.Object().which.has.size( 3 ).and.has.properties( "change", "properties", "model" );
			result.change.should.be.String().which.is.equal( "updated" );
			result.properties.should.be.Object().which.has.size( 3 ).and.has.properties( "uuid", "title", "prio" );
			result.properties.uuid.should.be.equal( original.uuid );
			result.properties.title.should.be.equal( "adjusted title" );
			result.properties.prio.should.be.equal( -321 );
			result.model.should.be.equal( "Todo" );
		} );

		it( "emits broadcast on server-side deletion of an instance of a server-side model", async() => {
			const original = new ctx.hitchy.api.models.Todo();
			original.title = "the title";
			original.prio = 546;
			await original.save();

			const result = await new Promise( ( resolve, reject ) => {
				const socket = io( ctx.url );

				socket.once( "connect_error", reject );
				socket.once( "todo:changed", record => {
					socket.close();
					resolve( record );
				} );

				socket.once( "connect", () => {
					const update = new ctx.hitchy.api.models.Todo( original.uuid );
					update.remove();
				} );
			} );

			result.should.be.Object().which.has.size( 3 ).and.has.properties( "change", "properties", "model" );
			result.change.should.be.String().which.is.equal( "deleted" );
			result.properties.should.be.Object().which.has.size( 1 ).and.has.properties( "uuid" );
			result.properties.uuid.should.be.equal( original.uuid );
			result.model.should.be.equal( "Todo" );
		} );
	} );

	describe( "with API for controlling data being enabled", () => {
		const ctx = {};

		afterEach( SDT.after( ctx ) );
		beforeEach( SDT.before( ctx, {
			projectFolder: false,
			plugin: true,
			files: {
				"config/auth.js": `exports.auth = { authorizations: { "@hitchy.odem": "*" } }`,
				"config/socket.js": `exports.socket = { odem: { crud: true } }`,
				"api/model/todo.js": `module.exports = { props: { title: {}, prio: { type: "number", index: "eq" } } }`,
			}
		} ) );

		beforeEach( () => {
			ctx.url = `http://127.0.0.1:${ctx.server.address().port}/hitchy/odem`;
		} );

		it( "emits broadcast on server-side creation of an instance of a server-side model", async() => {
			let original;

			const result = await new Promise( ( resolve, reject ) => {
				const socket = io( ctx.url );

				socket.once( "connect_error", reject );
				socket.once( "todo:changed", record => {
					socket.close();
					resolve( record );
				} );

				socket.once( "connect", () => {
					original = new ctx.hitchy.api.models.Todo();
					original.title = "another title";
					original.prio = 123;
					original.save();
				} );
			} );

			result.should.be.Object().which.has.size( 3 ).and.has.properties( "change", "properties", "model" );
			result.change.should.be.String().which.is.equal( "created" );
			result.properties.should.be.Object().which.has.size( 3 ).and.has.properties( "uuid", "title", "prio" );
			result.properties.uuid.should.be.equal( original.uuid );
			result.properties.title.should.be.equal( "another title" );
			result.properties.prio.should.be.equal( 123 );
			result.model.should.be.equal( "Todo" );
		} );

		it( "emits broadcast on server-side update of an instance of a server-side model", async() => {
			const original = new ctx.hitchy.api.models.Todo();
			original.title = "another title";
			original.prio = 123;
			await original.save();

			const result = await new Promise( ( resolve, reject ) => {
				const socket = io( ctx.url );

				socket.once( "connect_error", reject );
				socket.once( "todo:changed", record => {
					socket.close();
					resolve( record );
				} );

				socket.once( "connect", async() => {
					const update = new ctx.hitchy.api.models.Todo( original.uuid );
					await update.load();

					update.title = "adjusted title";
					update.prio = "-321";
					update.save();
				} );
			} );

			result.should.be.Object().which.has.size( 3 ).and.has.properties( "change", "properties", "model" );
			result.change.should.be.String().which.is.equal( "updated" );
			result.properties.should.be.Object().which.has.size( 3 ).and.has.properties( "uuid", "title", "prio" );
			result.properties.uuid.should.be.equal( original.uuid );
			result.properties.title.should.be.equal( "adjusted title" );
			result.properties.prio.should.be.equal( -321 );
			result.model.should.be.equal( "Todo" );
		} );

		it( "emits broadcast on server-side deletion of an instance of a server-side model", async() => {
			const original = new ctx.hitchy.api.models.Todo();
			original.title = "the title";
			original.prio = 546;
			await original.save();

			const result = await new Promise( ( resolve, reject ) => {
				const socket = io( ctx.url );

				socket.once( "connect_error", reject );
				socket.once( "todo:changed", record => {
					socket.close();
					resolve( record );
				} );

				socket.once( "connect", () => {
					const update = new ctx.hitchy.api.models.Todo( original.uuid );
					update.remove();
				} );
			} );

			result.should.be.Object().which.has.size( 3 ).and.has.properties( "change", "properties", "model" );
			result.change.should.be.String().which.is.equal( "deleted" );
			result.properties.should.be.Object().which.has.size( 1 ).and.has.properties( "uuid" );
			result.properties.uuid.should.be.equal( original.uuid );
			result.model.should.be.equal( "Todo" );
		} );
	} );
} );
