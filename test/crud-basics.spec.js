"use strict";

const { describe, it, beforeEach, afterEach } = require( "mocha" );
const SDT = require( "@hitchy/server-dev-tools" );
const { io } = require( "socket.io-client" );

require( "should" );

const modelFiles = {
	"api/model/todo.js": `module.exports = { 
		props: { 
			title: {}, 
			prio: { type: "number", protected: true }, 
			due: { type: "date", private: true }
		} }`,

	"api/model/task.js": `module.exports = {
		props: {
			label: { readonly: true },
			done: { type: "boolean", protected: true },
			valid: { type: "date", private: true }
		}, options: { expose: "protected" } }`,

	"api/model/secret.js": `module.exports = {
		props: {
			name: {},
			level: { type: "number", protected: true },
			code: { readonly: true, private: true }
		}, options: { expose: "private" } }`,
};

const enableControlFiles = {
	"config/socket.js": `exports.socket = { odem: { crud: true } }`,
};

const setup = async( ctx, authz, files ) => {
	await SDT.before( ctx, {
		projectFolder: false,
		plugin: true,
		// options: { debug: true },
		files: {
			...modelFiles,
			"config/auth.js": `exports.auth = { authorizations: ${JSON.stringify( authz || {} )} }`,
			...files || {}
		},
	} )();

	const user = new ctx.hitchy.api.model.User();
	user.name = "john.doe";
	await user.setPassword( "secret" );

	await user.save();

	ctx.url = `http://127.0.0.1:${ctx.server.address().port}/hitchy/odem`;
};

const query = async( socket, action, ...args ) => {
	return await new Promise( ( resolve, reject ) => {
		socket.once( "connect_error", reject );
		socket.once( "connect", () => {
			socket.emit( action, ...args || [] , record => {
				socket.close();
				resolve( record );
			} );
		} );
	} );
};

const createTodo = async ctx => {
	const todo = new ctx.hitchy.api.models.Todo();

	todo.title = "another title";
	todo.prio = 123;

	await todo.save();

	return todo;
};

const createTodos = async ctx => {
	const a = new ctx.hitchy.api.models.Todo();
	a.title = "first title";
	a.prio = -20;
	await a.save();

	const b = new ctx.hitchy.api.models.Todo();
	b.title = "second title";
	b.prio = 5.7;
	await b.save();

	const c = new ctx.hitchy.api.models.Todo();
	c.title = "third title";
	c.prio = 5.8;
	await c.save();

	return [ a, b, c ];
};

const publicAccess = {
	"@hitchy.odem": "*",
};

describe( "Controlling document-oriented database via websocket", () => {
	describe( "while according configuration option is disabled", () => {
		const ctx = {};

		afterEach( SDT.after( ctx ) );
		beforeEach( () => setup( ctx, publicAccess ) );

		it( "rejects to handle request for listing instances of a server-side model", async() => {
			( await query( io( ctx.url ), "todo:list", {}, false ) )
				.should.be.Object().which.has.size( 1 ).and.has.property( "error" )
				.which.match( /requested action is not available due to runtime configuration/ );
		} );

		it( "rejects to handle request for finding instances of a server-side model", async() => {
			( await query( io( ctx.url ), "todo:find", {}, {}, false ) )
				.should.be.Object().which.has.size( 1 ).and.has.property( "error" )
				.which.match( /requested action is not available due to runtime configuration/ );
		} );

		it( "rejects to handle request for creating an instance of a server-side model", async() => {
			( await query( io( ctx.url ), "todo:create", {
				title: "check creation request",
				prio: " 4 ",
			} ) )
				.should.be.Object().which.has.size( 1 ).and.has.property( "error" )
				.which.match( /requested action is not available due to runtime configuration/ );
		} );

		it( "rejects to handle request for reading an instance of a server-side model", async() => {
			const original = new ctx.hitchy.api.models.Todo();
			original.title = "its title";
			original.prio = 999.99;
			await original.save();

			( await query( io( ctx.url ), "todo:read", original.uuid ) )
				.should.be.Object().which.has.size( 1 ).and.has.property( "error" )
				.which.match( /requested action is not available due to runtime configuration/ );
		} );

		it( "rejects to handle request for updating an instance of a server-side model", async() => {
			const original = new ctx.hitchy.api.models.Todo();
			original.title = "old title";
			original.prio = 999.99;
			await original.save();

			( await query( io( ctx.url ), "todo:update", original.uuid, {
				prio: -40,
			} ) )
				.should.be.Object().which.has.size( 1 ).and.has.property( "error" )
				.which.match( /requested action is not available due to runtime configuration/ );

			const updated = new ctx.hitchy.api.models.Todo( original.uuid );
			await updated.load();

			updated.title.should.equal( "old title" );
			updated.prio.should.equal( 999.99 );
		} );

		it( "rejects to handle request for deleting an instance of a server-side model", async() => {
			const original = new ctx.hitchy.api.models.Todo();
			original.title = "another title";
			original.prio = 123;
			await original.save();

			( await query( io( ctx.url ), "todo:delete", original.uuid ) )
				.should.be.Object().which.has.size( 1 ).and.has.property( "error" )
				.which.match( /requested action is not available due to runtime configuration/ );

			const updated = new ctx.hitchy.api.models.Todo( original.uuid );
			await updated.load().should.be.resolved();

			updated.title.should.equal( "another title" );
			updated.prio.should.equal( 123 );
		} );
	} );

	describe( "while according configuration option is enabled", () => {
		const ctx = {};

		afterEach( SDT.after( ctx ) );
		beforeEach( () => setup( ctx, publicAccess, enableControlFiles ) );

		it( "handles request for listing instances of a server-side model w/ all properties", async() => {
			const [ a, b, c ] = await createTodos( ctx );

			( await query( io( ctx.url ), "todo:list", {}, true ) )
				.should.deepEqual( {
					success: true,
					count: 3,
					items: [
						{ ...a.toObject( { serialized: true } ) },
						{ ...b.toObject( { serialized: true } ) },
						{ ...c.toObject( { serialized: true } ) },
					]
				} );
		} );

		it( "handles request for listing instances of a server-side model w/o properties but UUIDs", async() => {
			const [ , b ] = await createTodos( ctx );

			( await query( io( ctx.url ), "todo:list", { offset: 1, limit: 1 }, false ) )
				.should.deepEqual( {
					success: true,
					count: 3,
					items: [
						{ uuid: b.uuid },
					]
				} );
		} );

		it( "handles request for finding instances of a server-side model w/ all properties", async() => {
			const [ a, b, c ] = await createTodos( ctx );

			( await query( io( ctx.url ), "todo:find", { lt: { prio: " 10 " } }, {}, true ) )
				.should.deepEqual( {
					success: true,
					count: 3,
					items: [
						{ ...a.toObject( { serialized: true } ) },
						{ ...b.toObject( { serialized: true } ) },
						{ ...c.toObject( { serialized: true } ) },
					],
				} );
		} );

		it( "handles request for finding instances of a server-side model w/o properties but UUIDs", async() => {
			const [ , b ] = await createTodos( ctx );

			( await query( io( ctx.url ), "todo:find", { gte: { prio: " -20 " } }, { limit: 1, offset: 1 }, false ) )
				.should.deepEqual( {
					success: true,
					count: 3,
					items: [
						{ uuid: b.uuid },
					]
				} );
		} );

		it( "handles request for creating an instance of a server-side model", async() => {
			const result = await query( io( ctx.url ), "todo:create", {
				title: "check creation request",
				prio: " 4 ",
			} );

			result.properties.uuid.should.be.String().which.match( /^[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12}$/i );
			result.should.deepEqual( {
				success: true,
				properties: {
					uuid: result.properties.uuid,
					title: "check creation request",
					prio: 4,
				}
			} );

			const item = new ctx.hitchy.api.models.Todo( result.properties.uuid );
			await item.load();

			item.title.should.be.equal( "check creation request" );
			item.prio.should.be.equal( 4 );
		} );

		it( "handles request for reading an instance of a server-side model", async() => {
			const todo = await createTodo( ctx );

			( await query( io( ctx.url ), "todo:read", todo.uuid ) )
				.should.deepEqual( {
					success: true,
					properties: { ...todo.toObject( { serialized: true } ) }
				} );
		} );

		it( "handles request for updating an instance of a server-side model", async() => {
			const todo = await createTodo( ctx );

			( await query( io( ctx.url ), "todo:update", todo.uuid, {
				title: "this is the new title",
				prio: 3.6,
			} ) )
				.should.deepEqual( {
					success: true,
					properties: {
						uuid: todo.uuid,
						title: "this is the new title",
						prio: 3.6
					}
				} );

			const updated = new ctx.hitchy.api.models.Todo( todo.uuid );
			await updated.load();

			updated.title.should.be.equal( "this is the new title" );
			updated.prio.should.be.equal( 3.6 );
		} );

		it( "handles request for deleting an instance of a server-side model", async() => {
			const todo = await createTodo( ctx );

			( await query( io( ctx.url ), "todo:delete", todo.uuid ) )
				.should.deepEqual( {
					success: true,
					properties: {
						uuid: todo.uuid
					}
				} );

			const updated = new ctx.hitchy.api.models.Todo( todo.uuid );
			await updated.load().should.be.rejected();
		} );
	} );
} );
