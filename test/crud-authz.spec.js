"use strict";

const { describe, it, beforeEach, afterEach } = require( "mocha" );
const SDT = require( "@hitchy/server-dev-tools" );
const { io } = require( "socket.io-client" );

require( "should" );

const modelFiles = {
	"api/model/todo.js": `module.exports = { 
		props: { 
			title: {}, 
			prio: { type: "number", protected: true }, 
			due: { type: "date", private: true }
		} }`,

	"api/model/task.js": `module.exports = {
		props: {
			label: { readonly: true },
			done: { type: "boolean", protected: true },
			valid: { type: "date", private: true }
		}, options: { expose: "protected" } }`,

	"api/model/secret.js": `module.exports = {
		props: {
			name: {},
			level: { type: "number", protected: true },
			code: { readonly: true, private: true }
		}, options: { expose: "private" } }`,
};

const setup = async( ctx, authz, files = undefined ) => {
	await SDT.before( ctx, {
		projectFolder: false,
		plugin: true,
		// options: { debug: true },
		files: {
			...modelFiles,
			"config/socket.js": `exports.socket = { odem: { crud: true } }`,
			"config/auth.js": `exports.auth = { authorizations: ${JSON.stringify( authz || {} )} }`,
			...files || {}
		},
	} )();

	const user = new ctx.hitchy.api.model.User();
	user.name = "john.doe";
	await user.setPassword( "secret" );

	await user.save();

	ctx.url = `http://127.0.0.1:${ctx.server.address().port}/hitchy/odem`;
};

const authenticatedSocket = async( ctx, name = undefined, password = undefined ) => {
	const options = {};

	if ( name != null && password != null ) {
		const res = await ctx.post( "/api/auth/login", `username=${name}&password=${password}`, {
			"content-type": "application/x-www-form-urlencoded",
		} );

		res.statusCode.should.equal( 200 );

		let sid = null;

		( res.headers["set-cookie"] || [] ).some( cookie => {
			const match = /^sessionId=\s*([^;]+)/.exec( cookie );

			if ( match ) {
				sid = match[1];
				return true;
			}

			return false;
		} );

		options.extraHeaders = {
			cookie: `sessionId=${sid}`
		};
	}

	return new Promise( ( resolve, reject ) => {
		const socket = io( ctx.url, options );

		socket.once( "connect_error", reject );
		socket.once( "connect", () => resolve( socket ) );
	} );
};

const connect = async( ctx, fn ) => {
	const sockets = await Promise.all( [
		authenticatedSocket( ctx ),
		authenticatedSocket( ctx, "john.doe", "secret" ),
		authenticatedSocket( ctx, "admin", "nimda" ),
	] );

	try {
		return await fn( ...sockets );
	} finally {
		sockets.forEach( socket => socket.close() );
	}
};

const query = async( socket, action, ...args ) => {
	return await new Promise( resolve => {
		socket.emit( action, ...args || [] , record => {
			resolve( record );
		} );
	} );
};

const createTodo = async( ctx, serialized = true ) => {
	const todo = new ctx.hitchy.api.models.Todo();

	todo.title = "todo title";
	todo.prio = 123;
	todo.due = new Date( Date.now() + 30 * 86400 * 1000 );

	await todo.save();

	return serialized ? todo.toObject( { serialized: true } ) : todo;
};

const createTask = async( ctx, serialized = true ) => {
	const task = new ctx.hitchy.api.models.Task();

	task.label = "task label";
	task.done = false;
	task.valid = new Date( Date.now() + 2 * 86400 * 1000 );

	await task.save();

	return serialized ? task.toObject( { serialized: true } ) : task;
};

const createSecret = async( ctx, serialized = true ) => {
	const secret = new ctx.hitchy.api.models.Secret();

	secret.name = "secret name";
	secret.level = 5;
	secret.code = "geheim!";

	await secret.save();

	return serialized ? secret.toObject( { serialized: true } ) : secret;
};

const serializeAll = list => list.map( item => item.toObject( { serialized: true } ) );

const sortBy = ( list, property ) => list.sort( ( l, r ) => Number( l[property] ) - Number( r[property] ) );

const loadSerialized = ( Model, uuid ) => new Model( uuid ).load().then( model => model.toObject( { serialized: true } ) );

const publicAccess = {
	"@hitchy.odem": "*",
};

const regularExposedAccess = {
	"@hitchy.odem.model.Todo.expose": "john.doe",
	"@hitchy.odem.model.Task.expose": "john.doe",
	"@hitchy.odem.model.Secret.expose": "john.doe",
};

const regularAccess = {
	...regularExposedAccess,
	"@hitchy.odem.model.Todo.list": "john.doe",
	"@hitchy.odem.model.Task.list": "john.doe",
	"@hitchy.odem.model.Secret.list": "john.doe",
	"@hitchy.odem.model.Todo.create": "john.doe",
	"@hitchy.odem.model.Task.create": "john.doe",
	"@hitchy.odem.model.Secret.create": "john.doe",
	"@hitchy.odem.model.Todo.read": "john.doe",
	"@hitchy.odem.model.Task.read": "john.doe",
	"@hitchy.odem.model.Secret.read": "john.doe",
	"@hitchy.odem.model.Todo.write": "john.doe",
	"@hitchy.odem.model.Task.write": "john.doe",
	"@hitchy.odem.model.Secret.write": "john.doe",
	"@hitchy.odem.model.Todo.remove": "john.doe",
	"@hitchy.odem.model.Task.remove": "john.doe",
	"@hitchy.odem.model.Secret.remove": "john.doe",
};

const regularPropertiesAccess = {
	...regularAccess,
	"@hitchy.odem.model.Todo.property.prio": "john.doe",
	"@hitchy.odem.model.Task.property.done": "john.doe",
	"@hitchy.odem.model.Secret.property.level": "john.doe",
};

describe( "Controlling document-oriented database via websocket", () => {
	const ctx = {};

	describe( "by default", () => {
		afterEach( SDT.after( ctx ) );
		beforeEach( () => setup( ctx ) );

		it( "rejects to fetch items of models with any level of exposure for non-privileged users", async() => {
			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:list", {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:list", {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "todo:list", {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:list", {}, true ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "task:list", {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:list", {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "task:list", {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:list", {}, true ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "secret:list", {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:list", {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "secret:list", {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:list", {}, true ) ).should.deepEqual( { error: "access forbidden" } );
			} );
		} );

		it( "fetches items of non-privately exposed models for privileged users", async() => {
			const [ todo, task ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:list", {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid }] } );
				( await query( admin, "task:list", {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid }] } );
				( await query( admin, "todo:list", {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid, title: todo.title, prio: todo.prio }] } );
				( await query( admin, "task:list", {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid, label: task.label, done: task.done }] } );

				( await query( admin, "secret:list", {}, false ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( admin, "secret:list", {}, true ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "rejects to find items of models with any level of exposure for non-privileged users", async() => {
			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:find", { neq: { title: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:find", { neq: { title: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "todo:find", { neq: { title: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:find", { neq: { title: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "task:find", { neq: { label: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:find", { neq: { label: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "task:find", { neq: { label: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:find", { neq: { label: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "secret:find", { neq: { name: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:find", { neq: { name: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "secret:find", { neq: { name: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:find", { neq: { name: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden" } );
			} );
		} );

		it( "finds items of non-privately exposed models for privileged users including all non-private properties", async() => {
			const [ todo, task ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:find", { neq: { title: "foo" } }, {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid }] } );
				( await query( admin, "task:find", { neq: { label: "foo" } }, {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid }] } );
				( await query( admin, "todo:find", { neq: { title: "foo" } }, {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid, title: todo.title, prio: todo.prio }] } );
				( await query( admin, "task:find", { neq: { label: "foo" } }, {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid, label: task.label, done: task.done }] } );

				( await query( admin, "secret:find", { neq: { name: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( admin, "secret:find", { neq: { name: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "rejects to create items of models with any level of exposure for non-privileged users", async() => {
			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:create", { title: "foo", prio: 1 } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:create", { title: "bar", prio: 2 } ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "task:create", { label: "Foo", done: 0 } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:create", { label: "Bar", done: 1 } ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "secret:create", { name: "fOO", level: 1 } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:create", { name: "bAR", level: 4 } ) ).should.deepEqual( { error: "access forbidden" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			( await Todo.list() ).should.deepEqual( [] );
			( await Task.list() ).should.deepEqual( [] );
			( await Secret.list() ).should.deepEqual( [] );
		} );

		it( "creates items of non-privately exposed models for privileged users considering non-private properties, only", async() => {
			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:create", { title: "foo", prio: 3, due: "2030-12-31T12:00:00" } ) ).should.containDeep( { success: true } );
				( await query( admin, "task:create", { label: "bar", done: 0, value: "2031-06-06T18:23.45" } ) ).should.containDeep( { success: true } );

				( await query( admin, "secret:read", { name: "bam", level: 4, code: "secret!" } ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			const todos = serializeAll( await Todo.list() );
			const tasks = serializeAll( await Task.list() );
			const secrets = serializeAll( await Secret.list() );

			todos.should.deepEqual( [{
				uuid: todos[0].uuid,
				title: "foo",
				prio: 3,
			}] );

			tasks.should.deepEqual( [{
				uuid: tasks[0].uuid,
				label: "bar", // <-- has been accepted on creation as intended though marked as readonly in model's definition
				done: 0,
			}] );

			secrets.should.deepEqual( [] );
		} );

		it( "rejects to read items of models with any level of exposure for non-privileged users", async() => {
			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:read", "" ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:read", "" ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "task:read", "" ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:read", "" ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "secret:read", "" ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:read", "" ) ).should.deepEqual( { error: "access forbidden" } );
			} );
		} );

		it( "reads items of non-privately exposed models for privileged users", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:read", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: todo.title, prio: todo.prio } } );
				( await query( admin, "task:read", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 0 } } );

				( await query( admin, "secret:read", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "rejects to update items of models with any level of exposure for non-privileged users", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:update", todo.uuid, { title: "title" } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:update", todo.uuid, { title: "title" } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "todo:write", todo.uuid, { title: "title" } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:write", todo.uuid, { title: "title" } ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "task:update", task.uuid, { done: !task.done } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:update", task.uuid, { done: !task.done } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "task:write", task.uuid, { done: !task.done } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:write", task.uuid, { done: !task.done } ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "secret:update", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:update", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "secret:write", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:write", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			( await loadSerialized( Todo, todo.uuid ) ).should.deepEqual( todo );
			( await loadSerialized( Task, task.uuid ) ).should.deepEqual( task );
			( await loadSerialized( Secret, secret.uuid ) ).should.deepEqual( secret );
		} );

		it( "updates items of non-privately exposed models for privileged users", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:update", todo.uuid, { title: "changed", prio: 456 } ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: "changed", prio: 456 } } );
				( await query( admin, "todo:write", todo.uuid, { title: "reverted", due: "2055-12-12T12:12:12" } ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: "reverted", prio: 456 } } );
				( await query( admin, "task:update", task.uuid, { label: "changed", done: !task.done } ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 1 } } );
				( await query( admin, "task:write", task.uuid, { label: "reverted", valid: "2060-01-01T00:30:00" } ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 1 } } );

				( await query( admin, "secret:update", secret.uuid, { name: "changed", level: 600 } ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( admin, "secret:write", secret.uuid, { name: "changed", level: 600 } ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			( await loadSerialized( Todo, todo.uuid ) ).should.deepEqual( { ...todo, title: "reverted", prio: 456 } );
			( await loadSerialized( Task, task.uuid ) ).should.deepEqual( { ...task, done: 1 } );
			( await loadSerialized( Secret, secret.uuid ) ).should.deepEqual( secret );
		} );

		it( "rejects to remove items of models with any level of exposure for non-privileged users", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:delete", todo.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:delete", todo.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "todo:remove", todo.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:remove", todo.uuid ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "task:delete", task.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:delete", task.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "task:remove", task.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:remove", task.uuid ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "secret:delete", secret.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:delete", secret.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "secret:remove", secret.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:remove", secret.uuid ) ).should.deepEqual( { error: "access forbidden" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			( await loadSerialized( Todo, todo.uuid ) ).should.deepEqual( todo );
			( await loadSerialized( Task, task.uuid ) ).should.deepEqual( task );
			( await loadSerialized( Secret, secret.uuid ) ).should.deepEqual( secret );
		} );

		it( "removes items of non-privately exposed models for privileged users", async() => {
			let [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:delete", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid } } );
				( await query( admin, "task:delete", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid } } );

				( await query( admin, "secret:delete", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			await loadSerialized( Todo, todo.uuid ).should.be.rejected();
			await loadSerialized( Task, task.uuid ).should.be.rejected();
			await loadSerialized( Secret, secret.uuid ).should.not.be.rejected();

			[ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:remove", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid } } );
				( await query( admin, "task:remove", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid } } );

				( await query( admin, "secret:remove", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			await loadSerialized( Todo, todo.uuid ).should.be.rejected();
			await loadSerialized( Task, task.uuid ).should.be.rejected();
			await loadSerialized( Secret, secret.uuid ).should.not.be.rejected();
		} );
	} );

	describe( "with access on all protected models and properties granted to the public", () => {
		afterEach( SDT.after( ctx ) );
		beforeEach( () => setup( ctx, publicAccess ) );

		it( "fetches items of non-privately exposed models for non-privileged users", async() => {
			const [ todo, task ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:list", {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid }] } );
				( await query( john, "todo:list", {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid }] } );
				( await query( guest, "todo:list", {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid, title: todo.title, prio: todo.prio }] } );
				( await query( john, "todo:list", {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid, title: todo.title, prio: todo.prio }] } );

				( await query( guest, "task:list", {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid }] } );
				( await query( john, "task:list", {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid }] } );
				( await query( guest, "task:list", {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid, label: task.label, done: task.done }] } );
				( await query( john, "task:list", {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid, label: task.label, done: task.done }] } );

				( await query( guest, "secret:list", {}, false ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( john, "secret:list", {}, false ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( guest, "secret:list", {}, true ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( john, "secret:list", {}, true ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "fetches items of non-privately exposed models for privileged users", async() => {
			const [ todo, task ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:list", {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid }] } );
				( await query( admin, "task:list", {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid }] } );
				( await query( admin, "todo:list", {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid, title: todo.title, prio: todo.prio }] } );
				( await query( admin, "task:list", {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid, label: task.label, done: task.done }] } );

				( await query( admin, "secret:list", {}, false ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( admin, "secret:list", {}, true ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "finds items of non-privately exposed models for non-privileged users", async() => {
			const [ todo, task ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:find", { neq: { title: "foo" } }, {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid }] } );
				( await query( john, "todo:find", { neq: { title: "foo" } }, {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid }] } );
				( await query( guest, "todo:find", { neq: { title: "foo" } }, {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid, title: todo.title, prio: todo.prio }] } );
				( await query( john, "todo:find", { neq: { title: "foo" } }, {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid, title: todo.title, prio: todo.prio }] } );

				( await query( guest, "task:find", { neq: { label: "foo" } }, {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid }] } );
				( await query( john, "task:find", { neq: { label: "foo" } }, {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid }] } );
				( await query( guest, "task:find", { neq: { label: "foo" } }, {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid, label: task.label, done: task.done }] } );
				( await query( john, "task:find", { neq: { label: "foo" } }, {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid, label: task.label, done: task.done }] } );

				( await query( guest, "secret:find", { neq: { name: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( john, "secret:find", { neq: { name: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( guest, "secret:find", { neq: { name: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( john, "secret:find", { neq: { name: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "finds items of non-privately exposed models for privileged users including all non-private properties", async() => {
			const [ todo, task ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:find", { neq: { title: "foo" } }, {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid }] } );
				( await query( admin, "task:find", { neq: { label: "foo" } }, {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid }] } );
				( await query( admin, "todo:find", { neq: { title: "foo" } }, {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid, title: todo.title, prio: todo.prio }] } );
				( await query( admin, "task:find", { neq: { label: "foo" } }, {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid, label: task.label, done: task.done }] } );

				( await query( admin, "secret:find", { neq: { name: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( admin, "secret:find", { neq: { name: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "creates items of non-privately exposed models for non-privileged users always accepting protected properties while ignoring private ones", async() => {
			const created = await connect( ctx, ( guest, john ) => Promise.all( [
				query( guest, "todo:create", { title: "foo", prio: 1, due: "2026-12-30T11:12:13" } ),
				query( john, "todo:create", { title: "bar", prio: 2, due: "2026-12-31T11:12:13" } ),

				query( guest, "task:create", { label: "Foo", done: 0, valid: "2027-13-14T10:10:10" } ),
				query( john, "task:create", { label: "Bar", done: 1, valid: "2027-13-15T10:10:10" } ),

				query( guest, "secret:create", { name: "fOO", level: 1, code: "secret!" } ),
				query( john, "secret:create", { name: "bAR", level: 4, code: "secret." } ),
			] ) );

			created.should.deepEqual( [
				{ success: true, properties: { uuid: created[0].properties.uuid, title: "foo", prio: 1 } },
				{ success: true, properties: { uuid: created[1].properties.uuid, title: "bar", prio: 2 } },
				{ success: true, properties: { uuid: created[2].properties.uuid, label: "Foo", done: 0 } },
				{ success: true, properties: { uuid: created[3].properties.uuid, label: "Bar", done: 1 } },
				{ error: "access forbidden by model" },
				{ error: "access forbidden by model" },
			] );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			const todos = sortBy( serializeAll( await Todo.list() ), "prio" );
			const tasks = sortBy( serializeAll( await Task.list() ), "done" );
			const secrets = sortBy( serializeAll( await Secret.list() ), "level" );

			todos.should.deepEqual( [ {
				uuid: todos[0].uuid,
				title: "foo",
				prio: 1,
			}, {
				uuid: todos[1].uuid,
				title: "bar",
				prio: 2,
			} ] );

			tasks.should.deepEqual( [ {
				uuid: tasks[0].uuid,
				label: "Foo", // <-- has been accepted on creation as intended though marked as readonly in model's definition
				done: 0,
			}, {
				uuid: tasks[1].uuid,
				label: "Bar", // <-- has been accepted on creation as intended though marked as readonly in model's definition
				done: 1,
			} ] );

			secrets.should.deepEqual( [] );
		} );

		it( "creates items of non-privately exposed models for privileged users considering non-private properties, only", async() => {
			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:create", { title: "foo", prio: 3, due: "2030-12-31T12:00:00" } ) ).should.containDeep( { success: true } );
				( await query( admin, "task:create", { label: "bar", done: 0, value: "2031-06-06T18:23.45" } ) ).should.containDeep( { success: true } );

				( await query( admin, "secret:read", { name: "bam", level: 4, code: "secret!" } ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			const todos = serializeAll( await Todo.list() );
			const tasks = serializeAll( await Task.list() );
			const secrets = serializeAll( await Secret.list() );

			todos.should.deepEqual( [{
				uuid: todos[0].uuid,
				title: "foo",
				prio: 3,
			}] );

			tasks.should.deepEqual( [{
				uuid: tasks[0].uuid,
				label: "bar", // <-- has been accepted on creation as intended though marked as readonly in model's definition
				done: 0,
			}] );

			secrets.should.deepEqual( [] );
		} );

		it( "reads items of non-privately exposed models for non-privileged users including all non-private properties", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:read", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: todo.title, prio: todo.prio } } );
				( await query( john, "todo:read", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: todo.title, prio: todo.prio } } );

				( await query( guest, "task:read", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: task.done } } );
				( await query( john, "task:read", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: task.done } } );

				( await query( guest, "secret:read", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( john, "secret:read", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "reads items of non-privately exposed models for privileged users", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:read", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: todo.title, prio: todo.prio } } );
				( await query( admin, "task:read", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 0 } } );

				( await query( admin, "secret:read", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "updates items of non-privately exposed models for non-privileged users accepting protected properties while ignoring private ones", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:update", todo.uuid, { title: "a", prio: 1, due: "2055-12-12T00:00:01" } ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: "a", prio: 1 } } );
				( await query( john, "todo:update", todo.uuid, { title: "b", prio: 2, due: "2055-12-12T00:00:02" } ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: "b", prio: 2 } } );
				( await query( guest, "todo:write", todo.uuid, { title: "c", prio: 3, due: "2055-12-12T00:00:03" } ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: "c", prio: 3 } } );
				( await query( john, "todo:write", todo.uuid, { title: "d", prio: 4, due: "2055-12-12T00:00:04" } ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: "d", prio: 4 } } );

				( await query( guest, "task:update", task.uuid, { label: "a", done: 1 } ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 1 } } );
				( await query( john, "task:update", task.uuid, { label: "b", done: 0 } ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 0 } } );
				( await query( guest, "task:write", task.uuid, { label: "c", done: 1 } ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 1 } } );
				( await query( john, "task:write", task.uuid, { label: "d", done: 0 } ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 0 } } );

				( await query( guest, "secret:update", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( john, "secret:update", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( guest, "secret:write", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( john, "secret:write", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			( await loadSerialized( Todo, todo.uuid ) ).should.deepEqual( { uuid: todo.uuid, title: "d", prio: 4, due: todo.due } );
			( await loadSerialized( Task, task.uuid ) ).should.deepEqual( { uuid: task.uuid, label: task.label, done: 0, valid: task.valid } );
			( await loadSerialized( Secret, secret.uuid ) ).should.deepEqual( secret );
		} );

		it( "updates items of non-privately exposed models for privileged users", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:update", todo.uuid, { title: "changed", prio: 456 } ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: "changed", prio: 456 } } );
				( await query( admin, "todo:write", todo.uuid, { title: "reverted", due: "2055-12-12T12:12:12" } ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: "reverted", prio: 456 } } );
				( await query( admin, "task:update", task.uuid, { label: "changed", done: !task.done } ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 1 } } );
				( await query( admin, "task:write", task.uuid, { label: "reverted", valid: "2060-01-01T00:30:00" } ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 1 } } );

				( await query( admin, "secret:update", secret.uuid, { name: "changed", level: 600 } ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( admin, "secret:write", secret.uuid, { name: "changed", level: 600 } ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			( await loadSerialized( Todo, todo.uuid ) ).should.deepEqual( { ...todo, title: "reverted", prio: 456 } );
			( await loadSerialized( Task, task.uuid ) ).should.deepEqual( { ...task, done: 1 } );
			( await loadSerialized( Secret, secret.uuid ) ).should.deepEqual( secret );
		} );

		it( "removes items of non-privately exposed models for non-privileged users", async() => {
			let [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:delete", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid } } );
				todo = await createTodo( ctx );
				( await query( john, "todo:delete", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid } } );
				todo = await createTodo( ctx );
				( await query( guest, "todo:remove", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid } } );
				todo = await createTodo( ctx );
				( await query( john, "todo:remove", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid } } );

				( await query( guest, "task:delete", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid } } );
				task = await createTask( ctx );
				( await query( john, "task:delete", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid } } );
				task = await createTask( ctx );
				( await query( guest, "task:remove", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid } } );
				task = await createTask( ctx );
				( await query( john, "task:remove", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid } } );

				( await query( guest, "secret:delete", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( john, "secret:delete", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( guest, "secret:remove", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( john, "secret:remove", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			( await Todo.list() ).should.deepEqual( [] );
			( await Task.list() ).should.deepEqual( [] );
			( await Secret.list() ).should.have.length( 1 );
		} );

		it( "removes items of non-privately exposed models for privileged users", async() => {
			let [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:delete", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid } } );
				( await query( admin, "task:delete", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid } } );

				( await query( admin, "secret:delete", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			await new Todo( todo.uuid ).load().should.be.rejected();
			await new Task( task.uuid ).load().should.be.rejected();
			await new Secret( secret.uuid ).load().should.not.be.rejected();

			[ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:remove", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid } } );
				( await query( admin, "task:remove", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid } } );

				( await query( admin, "secret:remove", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			await new Todo( todo.uuid ).load().should.be.rejected();
			await new Task( task.uuid ).load().should.be.rejected();
			await new Secret( secret.uuid ).load().should.not.be.rejected();
		} );
	} );

	describe( "with all models basically exposed to a particular user", () => {
		afterEach( SDT.after( ctx ) );
		beforeEach( () => setup( ctx, regularExposedAccess ) );

		it( "rejects to fetch items of non-privately exposed models for non-privileged users", async() => {
			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:list", {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:list", {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "todo:list", {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:list", {}, true ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "task:list", {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:list", {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "task:list", {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:list", {}, true ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "secret:list", {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:list", {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "secret:list", {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:list", {}, true ) ).should.deepEqual( { error: "access forbidden" } );
			} );
		} );

		it( "fetches items of non-privately exposed models for privileged users", async() => {
			const [ todo, task ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:list", {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid }] } );
				( await query( admin, "task:list", {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid }] } );
				( await query( admin, "todo:list", {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid, title: todo.title, prio: todo.prio }] } );
				( await query( admin, "task:list", {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid, label: task.label, done: task.done }] } );

				( await query( admin, "secret:list", {}, false ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( admin, "secret:list", {}, true ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "rejects to find items of non-privately exposed models for non-privileged users", async() => {
			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:find", { neq: { title: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:find", { neq: { title: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "todo:find", { neq: { title: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:find", { neq: { title: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "task:find", { neq: { label: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:find", { neq: { label: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "task:find", { neq: { label: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:find", { neq: { label: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "secret:find", { neq: { name: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:find", { neq: { name: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "secret:find", { neq: { name: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:find", { neq: { name: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden" } );
			} );
		} );

		it( "finds items of non-privately exposed models for privileged users including all non-private properties", async() => {
			const [ todo, task ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:find", { neq: { title: "foo" } }, {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid }] } );
				( await query( admin, "task:find", { neq: { label: "foo" } }, {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid }] } );
				( await query( admin, "todo:find", { neq: { title: "foo" } }, {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid, title: todo.title, prio: todo.prio }] } );
				( await query( admin, "task:find", { neq: { label: "foo" } }, {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid, label: task.label, done: task.done }] } );

				( await query( admin, "secret:find", { neq: { name: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( admin, "secret:find", { neq: { name: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "rejects to create items of non-privately exposed models for non-privileged users", async() => {
			const created = await connect( ctx, ( guest, john ) => Promise.all( [
				query( guest, "todo:create", { title: "foo", prio: 1, due: "2026-12-30T11:12:13" } ),
				query( john, "todo:create", { title: "bar", prio: 2, due: "2026-12-31T11:12:13" } ),

				query( guest, "task:create", { label: "Foo", done: 0, valid: "2027-13-14T10:10:10" } ),
				query( john, "task:create", { label: "Bar", done: 1, valid: "2027-13-15T10:10:10" } ),

				query( guest, "secret:create", { name: "fOO", level: 1, code: "secret!" } ),
				query( john, "secret:create", { name: "bAR", level: 4, code: "secret." } ),
			] ) );

			created.should.deepEqual( [
				{ error: "access forbidden" },
				{ error: "access forbidden" },
				{ error: "access forbidden" },
				{ error: "access forbidden" },
				{ error: "access forbidden" },
				{ error: "access forbidden" },
			] );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			const todos = serializeAll( await Todo.list() );
			const tasks = serializeAll( await Task.list() );
			const secrets = serializeAll( await Secret.list() );

			todos.should.deepEqual( [] );
			tasks.should.deepEqual( [] );
			secrets.should.deepEqual( [] );
		} );

		it( "creates items of non-privately exposed models for privileged users considering non-private properties, only", async() => {
			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:create", { title: "foo", prio: 3, due: "2030-12-31T12:00:00" } ) ).should.containDeep( { success: true } );
				( await query( admin, "task:create", { label: "bar", done: 0, value: "2031-06-06T18:23.45" } ) ).should.containDeep( { success: true } );

				( await query( admin, "secret:read", { name: "bam", level: 4, code: "secret!" } ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			const todos = serializeAll( await Todo.list() );
			const tasks = serializeAll( await Task.list() );
			const secrets = serializeAll( await Secret.list() );

			todos.should.deepEqual( [{
				uuid: todos[0].uuid,
				title: "foo",
				prio: 3,
			}] );

			tasks.should.deepEqual( [{
				uuid: tasks[0].uuid,
				label: "bar", // <-- has been accepted on creation as intended though marked as readonly in model's definition
				done: 0,
			}] );

			secrets.should.deepEqual( [] );
		} );

		it( "rejects to read items of non-privately exposed models for non-privileged users", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:read", todo.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:read", todo.uuid ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "task:read", task.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:read", task.uuid ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "secret:read", secret.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:read", secret.uuid ) ).should.deepEqual( { error: "access forbidden" } );
			} );
		} );

		it( "reads items of non-privately exposed models for privileged users", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:read", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: todo.title, prio: todo.prio } } );
				( await query( admin, "task:read", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 0 } } );

				( await query( admin, "secret:read", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "rejects to update items of non-privately exposed models for non-privileged users", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:update", todo.uuid, { title: "a", prio: 1, due: "2055-12-12T00:00:01" } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:update", todo.uuid, { title: "b", prio: 2, due: "2055-12-12T00:00:02" } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "todo:write", todo.uuid, { title: "c", prio: 3, due: "2055-12-12T00:00:03" } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:write", todo.uuid, { title: "d", prio: 4, due: "2055-12-12T00:00:04" } ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "task:update", task.uuid, { label: "a", done: 1 } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:update", task.uuid, { label: "b", done: 0 } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "task:write", task.uuid, { label: "c", done: 1 } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:write", task.uuid, { label: "d", done: 0 } ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "secret:update", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:update", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "secret:write", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:write", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			( await loadSerialized( Todo, todo.uuid ) ).should.deepEqual( todo );
			( await loadSerialized( Task, task.uuid ) ).should.deepEqual( task );
			( await loadSerialized( Secret, secret.uuid ) ).should.deepEqual( secret );
		} );

		it( "updates items of non-privately exposed models for privileged users", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:update", todo.uuid, { title: "changed", prio: 456 } ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: "changed", prio: 456 } } );
				( await query( admin, "todo:write", todo.uuid, { title: "reverted", due: "2055-12-12T12:12:12" } ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: "reverted", prio: 456 } } );
				( await query( admin, "task:update", task.uuid, { label: "changed", done: !task.done } ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 1 } } );
				( await query( admin, "task:write", task.uuid, { label: "reverted", valid: "2060-01-01T00:30:00" } ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 1 } } );

				( await query( admin, "secret:update", secret.uuid, { name: "changed", level: 600 } ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( admin, "secret:write", secret.uuid, { name: "changed", level: 600 } ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			( await loadSerialized( Todo, todo.uuid ) ).should.deepEqual( { ...todo, title: "reverted", prio: 456 } );
			( await loadSerialized( Task, task.uuid ) ).should.deepEqual( { ...task, done: 1 } );
			( await loadSerialized( Secret, secret.uuid ) ).should.deepEqual( secret );
		} );

		it( "rejects to remove items of non-privately exposed models for non-privileged users", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:delete", todo.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:delete", todo.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "todo:remove", todo.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:remove", todo.uuid ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "task:delete", task.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:delete", task.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "task:remove", task.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:remove", task.uuid ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "secret:delete", secret.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:delete", secret.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( guest, "secret:remove", secret.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:remove", secret.uuid ) ).should.deepEqual( { error: "access forbidden" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			serializeAll( await Todo.list() ).should.deepEqual( [todo] );
			serializeAll( await Task.list() ).should.deepEqual( [task] );
			serializeAll( await Secret.list() ).should.deepEqual( [secret] );
		} );

		it( "removes items of non-privately exposed models for privileged users", async() => {
			let [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:delete", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid } } );
				( await query( admin, "task:delete", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid } } );

				( await query( admin, "secret:delete", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			await new Todo( todo.uuid ).load().should.be.rejected();
			await new Task( task.uuid ).load().should.be.rejected();
			await new Secret( secret.uuid ).load().should.not.be.rejected();

			[ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:remove", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid } } );
				( await query( admin, "task:remove", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid } } );

				( await query( admin, "secret:remove", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			await new Todo( todo.uuid ).load().should.be.rejected();
			await new Task( task.uuid ).load().should.be.rejected();
			await new Secret( secret.uuid ).load().should.not.be.rejected();
		} );
	} );

	describe( "with actions on all additionally exposed models granted to a particular user", () => {
		afterEach( SDT.after( ctx ) );
		beforeEach( () => setup( ctx, regularAccess ) );

		it( "fetches items of non-privately exposed models for non-privileged users, but rejects it from guests", async() => {
			const [ todo, task ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:list", {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:list", {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid }] } );
				( await query( guest, "todo:list", {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:list", {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid, title: todo.title }] } );

				( await query( guest, "task:list", {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:list", {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid }] } );
				( await query( guest, "task:list", {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:list", {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid, label: task.label }] } );

				( await query( guest, "secret:list", {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:list", {}, false ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( guest, "secret:list", {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:list", {}, true ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "fetches items of non-privately exposed models for privileged users", async() => {
			const [ todo, task ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:list", {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid }] } );
				( await query( admin, "task:list", {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid }] } );
				( await query( admin, "todo:list", {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid, title: todo.title, prio: todo.prio }] } );
				( await query( admin, "task:list", {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid, label: task.label, done: task.done }] } );

				( await query( admin, "secret:list", {}, false ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( admin, "secret:list", {}, true ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "finds items of non-privately exposed models for non-privileged users, but rejects it from guests", async() => {
			const [ todo, task ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:find", { neq: { title: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:find", { neq: { title: "foo" } }, {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid }] } );
				( await query( guest, "todo:find", { neq: { title: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:find", { neq: { title: "foo" } }, {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid, title: todo.title }] } );

				( await query( guest, "task:find", { neq: { label: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:find", { neq: { label: "foo" } }, {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid }] } );
				( await query( guest, "task:find", { neq: { label: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:find", { neq: { label: "foo" } }, {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid, label: task.label }] } );

				( await query( guest, "secret:find", { neq: { name: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:find", { neq: { name: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( guest, "secret:find", { neq: { name: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:find", { neq: { name: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "finds items of non-privately exposed models for privileged users including all non-private properties", async() => {
			const [ todo, task ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:find", { neq: { title: "foo" } }, {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid }] } );
				( await query( admin, "task:find", { neq: { label: "foo" } }, {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid }] } );
				( await query( admin, "todo:find", { neq: { title: "foo" } }, {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid, title: todo.title, prio: todo.prio }] } );
				( await query( admin, "task:find", { neq: { label: "foo" } }, {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid, label: task.label, done: task.done }] } );

				( await query( admin, "secret:find", { neq: { name: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( admin, "secret:find", { neq: { name: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "creates items of non-privately exposed models for non-privileged users ignoring protected and private ones while rejecting guests", async() => {
			const created = await connect( ctx, ( guest, john ) => Promise.all( [
				query( guest, "todo:create", { title: "foo", prio: 1, due: "2026-12-30T11:12:13" } ),
				query( john, "todo:create", { title: "bar", prio: 2, due: "2026-12-31T11:12:13" } ),

				query( guest, "task:create", { label: "Foo", done: 0, valid: "2027-13-14T10:10:10" } ),
				query( john, "task:create", { label: "Bar", done: 1, valid: "2027-13-15T10:10:10" } ),

				query( guest, "secret:create", { name: "fOO", level: 1, code: "secret!" } ),
				query( john, "secret:create", { name: "bAR", level: 4, code: "secret." } ),
			] ) );

			created.should.deepEqual( [
				{ error: "access forbidden" },
				{ success: true, properties: { uuid: created[1].properties.uuid, title: "bar" } },
				{ error: "access forbidden" },
				{ success: true, properties: { uuid: created[3].properties.uuid, label: "Bar" } },
				{ error: "access forbidden" },
				{ error: "access forbidden by model" },
			] );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			const todos = sortBy( serializeAll( await Todo.list() ), "prio" );
			const tasks = sortBy( serializeAll( await Task.list() ), "done" );
			const secrets = sortBy( serializeAll( await Secret.list() ), "level" );

			todos.should.deepEqual( [{
				uuid: todos[0].uuid,
				title: "bar",
			}] );

			tasks.should.deepEqual( [{
				uuid: tasks[0].uuid,
				label: "Bar", // <-- has been accepted on creation as intended though marked as readonly in model's definition
			}] );

			secrets.should.deepEqual( [] );
		} );

		it( "creates items of non-privately exposed models for privileged users considering non-private properties, only", async() => {
			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:create", { title: "foo", prio: 3, due: "2030-12-31T12:00:00" } ) ).should.containDeep( { success: true } );
				( await query( admin, "task:create", { label: "bar", done: 0, value: "2031-06-06T18:23.45" } ) ).should.containDeep( { success: true } );

				( await query( admin, "secret:read", { name: "bam", level: 4, code: "secret!" } ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			const todos = serializeAll( await Todo.list() );
			const tasks = serializeAll( await Task.list() );
			const secrets = serializeAll( await Secret.list() );

			todos.should.deepEqual( [{
				uuid: todos[0].uuid,
				title: "foo",
				prio: 3,
			}] );

			tasks.should.deepEqual( [{
				uuid: tasks[0].uuid,
				label: "bar", // <-- has been accepted on creation as intended though marked as readonly in model's definition
				done: 0,
			}] );

			secrets.should.deepEqual( [] );
		} );

		it( "reads items of non-privately exposed models for non-privileged users excluding all non-public properties while rejecting guests", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:read", todo.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:read", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: todo.title } } );

				( await query( guest, "task:read", task.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:read", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label } } );

				( await query( guest, "secret:read", secret.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:read", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "reads items of non-privately exposed models for privileged users", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:read", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: todo.title, prio: todo.prio } } );
				( await query( admin, "task:read", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 0 } } );

				( await query( admin, "secret:read", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "updates items of non-privately exposed models for non-privileged users ignoring protected and private properties while rejecting guests", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( john, "todo:update", todo.uuid, { title: "a", prio: 2, due: "2055-12-12T00:00:02" } ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: "a" } } );
				( await query( guest, "todo:update", todo.uuid, { title: "b", prio: 1, due: "2055-12-12T00:00:01" } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:write", todo.uuid, { title: "c", prio: 4, due: "2055-12-12T00:00:04" } ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: "c" } } );
				( await query( guest, "todo:write", todo.uuid, { title: "d", prio: 3, due: "2055-12-12T00:00:03" } ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "task:update", task.uuid, { label: "a", done: 1 } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:update", task.uuid, { label: "b", done: 0 } ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label } } );
				( await query( guest, "task:write", task.uuid, { label: "c", done: 1 } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:write", task.uuid, { label: "d", done: 0 } ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label } } );

				( await query( guest, "secret:update", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:update", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( guest, "secret:write", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:write", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			( await loadSerialized( Todo, todo.uuid ) ).should.deepEqual( { uuid: todo.uuid, title: "c", prio: todo.prio, due: todo.due } );
			( await loadSerialized( Task, task.uuid ) ).should.deepEqual( { uuid: task.uuid, label: task.label, done: task.done, valid: task.valid } );
			( await loadSerialized( Secret, secret.uuid ) ).should.deepEqual( secret );
		} );

		it( "updates items of non-privately exposed models for privileged users", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:update", todo.uuid, { title: "changed", prio: 456 } ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: "changed", prio: 456 } } );
				( await query( admin, "todo:write", todo.uuid, { title: "reverted", due: "2055-12-12T12:12:12" } ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: "reverted", prio: 456 } } );
				( await query( admin, "task:update", task.uuid, { label: "changed", done: !task.done } ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 1 } } );
				( await query( admin, "task:write", task.uuid, { label: "reverted", valid: "2060-01-01T00:30:00" } ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 1 } } );

				( await query( admin, "secret:update", secret.uuid, { name: "changed", level: 600 } ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( admin, "secret:write", secret.uuid, { name: "changed", level: 600 } ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			( await loadSerialized( Todo, todo.uuid ) ).should.deepEqual( { ...todo, title: "reverted", prio: 456 } );
			( await loadSerialized( Task, task.uuid ) ).should.deepEqual( { ...task, done: 1 } );
			( await loadSerialized( Secret, secret.uuid ) ).should.deepEqual( secret );
		} );

		it( "removes items of non-privately exposed models for non-privileged users while rejecting guests", async() => {
			let [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:delete", todo.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:delete", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid } } );
				todo = await createTodo( ctx );
				( await query( guest, "todo:remove", todo.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:remove", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid } } );

				( await query( guest, "task:delete", task.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:delete", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid } } );
				task = await createTask( ctx );
				( await query( guest, "task:remove", task.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:remove", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid } } );

				( await query( guest, "secret:delete", secret.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:delete", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( guest, "secret:remove", secret.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:remove", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			( await Todo.list() ).should.deepEqual( [] );
			( await Task.list() ).should.deepEqual( [] );
			( await Secret.list() ).should.have.length( 1 );
		} );

		it( "removes items of non-privately exposed models for privileged users", async() => {
			let [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:delete", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid } } );
				( await query( admin, "task:delete", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid } } );

				( await query( admin, "secret:delete", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			await new Todo( todo.uuid ).load().should.be.rejected();
			await new Task( task.uuid ).load().should.be.rejected();
			await new Secret( secret.uuid ).load().should.not.be.rejected();

			[ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:remove", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid } } );
				( await query( admin, "task:remove", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid } } );

				( await query( admin, "secret:remove", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			await new Todo( todo.uuid ).load().should.be.rejected();
			await new Task( task.uuid ).load().should.be.rejected();
			await new Secret( secret.uuid ).load().should.not.be.rejected();
		} );
	} );

	describe( "with actions on all additionally exposed models and all protected properties granted to a regular user", () => {
		afterEach( SDT.after( ctx ) );
		beforeEach( () => setup( ctx, regularPropertiesAccess ) );

		it( "fetches items of non-privately exposed models for non-privileged users while rejecting guests", async() => {
			const [ todo, task ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:list", {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:list", {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid }] } );
				( await query( guest, "todo:list", {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:list", {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid, title: todo.title, prio: todo.prio }] } );

				( await query( guest, "task:list", {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:list", {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid }] } );
				( await query( guest, "task:list", {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:list", {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid, label: task.label, done: task.done }] } );

				( await query( guest, "secret:list", {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:list", {}, false ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( guest, "secret:list", {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:list", {}, true ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "fetches items of non-privately exposed models for privileged users", async() => {
			const [ todo, task ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:list", {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid }] } );
				( await query( admin, "task:list", {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid }] } );
				( await query( admin, "todo:list", {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid, title: todo.title, prio: todo.prio }] } );
				( await query( admin, "task:list", {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid, label: task.label, done: task.done }] } );

				( await query( admin, "secret:list", {}, false ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( admin, "secret:list", {}, true ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "finds items of non-privately exposed models for non-privileged users while rejecting guests", async() => {
			const [ todo, task ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:find", { neq: { title: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:find", { neq: { title: "foo" } }, {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid }] } );
				( await query( guest, "todo:find", { neq: { title: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:find", { neq: { title: "foo" } }, {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid, title: todo.title, prio: todo.prio }] } );

				( await query( guest, "task:find", { neq: { label: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:find", { neq: { label: "foo" } }, {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid }] } );
				( await query( guest, "task:find", { neq: { label: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:find", { neq: { label: "foo" } }, {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid, label: task.label, done: task.done }] } );

				( await query( guest, "secret:find", { neq: { name: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:find", { neq: { name: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( guest, "secret:find", { neq: { name: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:find", { neq: { name: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "finds items of non-privately exposed models for privileged users including all non-private properties", async() => {
			const [ todo, task ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:find", { neq: { title: "foo" } }, {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid }] } );
				( await query( admin, "task:find", { neq: { label: "foo" } }, {}, false ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid }] } );
				( await query( admin, "todo:find", { neq: { title: "foo" } }, {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: todo.uuid, title: todo.title, prio: todo.prio }] } );
				( await query( admin, "task:find", { neq: { label: "foo" } }, {}, true ) ).should.deepEqual( { success: true, count: 1, items: [{ uuid: task.uuid, label: task.label, done: task.done }] } );

				( await query( admin, "secret:find", { neq: { name: "foo" } }, {}, false ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( admin, "secret:find", { neq: { name: "foo" } }, {}, true ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "creates items of non-privately exposed models for non-privileged users always accepting protected properties while ignoring private ones and rejecting guests", async() => {
			const created = await connect( ctx, ( guest, john ) => Promise.all( [
				query( guest, "todo:create", { title: "foo", prio: 1, due: "2026-12-30T11:12:13" } ),
				query( john, "todo:create", { title: "bar", prio: 2, due: "2026-12-31T11:12:13" } ),

				query( guest, "task:create", { label: "Foo", done: 0, valid: "2027-13-14T10:10:10" } ),
				query( john, "task:create", { label: "Bar", done: 1, valid: "2027-13-15T10:10:10" } ),

				query( guest, "secret:create", { name: "fOO", level: 1, code: "secret!" } ),
				query( john, "secret:create", { name: "bAR", level: 4, code: "secret." } ),
			] ) );

			created.should.deepEqual( [
				{ error: "access forbidden" },
				{ success: true, properties: { uuid: created[1].properties.uuid, title: "bar", prio: 2 } },
				{ error: "access forbidden" },
				{ success: true, properties: { uuid: created[3].properties.uuid, label: "Bar", done: 1 } },
				{ error: "access forbidden" },
				{ error: "access forbidden by model" },
			] );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			const todos = sortBy( serializeAll( await Todo.list() ), "prio" );
			const tasks = sortBy( serializeAll( await Task.list() ), "done" );
			const secrets = sortBy( serializeAll( await Secret.list() ), "level" );

			todos.should.deepEqual( [{
				uuid: todos[0].uuid,
				title: "bar",
				prio: 2,
			}] );

			tasks.should.deepEqual( [{
				uuid: tasks[0].uuid,
				label: "Bar", // <-- has been accepted on creation as intended though marked as readonly in model's definition
				done: 1,
			}] );

			secrets.should.deepEqual( [] );
		} );

		it( "creates items of non-privately exposed models for privileged users considering non-private properties, only", async() => {
			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:create", { title: "foo", prio: 3, due: "2030-12-31T12:00:00" } ) ).should.containDeep( { success: true } );
				( await query( admin, "task:create", { label: "bar", done: 0, value: "2031-06-06T18:23.45" } ) ).should.containDeep( { success: true } );

				( await query( admin, "secret:read", { name: "bam", level: 4, code: "secret!" } ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			const todos = serializeAll( await Todo.list() );
			const tasks = serializeAll( await Task.list() );
			const secrets = serializeAll( await Secret.list() );

			todos.should.deepEqual( [{
				uuid: todos[0].uuid,
				title: "foo",
				prio: 3,
			}] );

			tasks.should.deepEqual( [{
				uuid: tasks[0].uuid,
				label: "bar", // <-- has been accepted on creation as intended though marked as readonly in model's definition
				done: 0,
			}] );

			secrets.should.deepEqual( [] );
		} );

		it( "reads items of non-privately exposed models for non-privileged users including all non-private properties while rejecting guests", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:read", todo.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:read", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: todo.title, prio: todo.prio } } );

				( await query( guest, "task:read", task.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:read", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: task.done } } );

				( await query( guest, "secret:read", secret.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:read", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "reads items of non-privately exposed models for privileged users", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:read", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: todo.title, prio: todo.prio } } );
				( await query( admin, "task:read", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 0 } } );

				( await query( admin, "secret:read", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );
		} );

		it( "updates items of non-privately exposed models for non-privileged users accepting protected properties while ignoring private ones and rejecting guests", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( john, "todo:update", todo.uuid, { title: "a", prio: 1, due: "2055-12-12T00:00:01" } ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: "a", prio: 1 } } );
				( await query( guest, "todo:update", todo.uuid, { title: "b", prio: 2, due: "2055-12-12T00:00:02" } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:write", todo.uuid, { title: "c", prio: 3, due: "2055-12-12T00:00:03" } ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: "c", prio: 3 } } );
				( await query( guest, "todo:write", todo.uuid, { title: "d", prio: 4, due: "2055-12-12T00:00:04" } ) ).should.deepEqual( { error: "access forbidden" } );

				( await query( guest, "task:update", task.uuid, { label: "a", done: 1 } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:update", task.uuid, { label: "b", done: 0 } ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 0 } } );
				( await query( guest, "task:write", task.uuid, { label: "c", done: 1 } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:write", task.uuid, { label: "d", done: 0 } ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 0 } } );

				( await query( guest, "secret:update", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:update", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( guest, "secret:write", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:write", secret.uuid, { name: "changed" } ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			( await loadSerialized( Todo, todo.uuid ) ).should.deepEqual( { uuid: todo.uuid, title: "c", prio: 3, due: todo.due } );
			( await loadSerialized( Task, task.uuid ) ).should.deepEqual( { uuid: task.uuid, label: task.label, done: 0, valid: task.valid } );
			( await loadSerialized( Secret, secret.uuid ) ).should.deepEqual( secret );
		} );

		it( "updates items of non-privately exposed models for privileged users", async() => {
			const [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:update", todo.uuid, { title: "changed", prio: 456 } ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: "changed", prio: 456 } } );
				( await query( admin, "todo:write", todo.uuid, { title: "reverted", due: "2055-12-12T12:12:12" } ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid, title: "reverted", prio: 456 } } );
				( await query( admin, "task:update", task.uuid, { label: "changed", done: !task.done } ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 1 } } );
				( await query( admin, "task:write", task.uuid, { label: "reverted", valid: "2060-01-01T00:30:00" } ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid, label: task.label, done: 1 } } );

				( await query( admin, "secret:update", secret.uuid, { name: "changed", level: 600 } ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( admin, "secret:write", secret.uuid, { name: "changed", level: 600 } ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			( await loadSerialized( Todo, todo.uuid ) ).should.deepEqual( { ...todo, title: "reverted", prio: 456 } );
			( await loadSerialized( Task, task.uuid ) ).should.deepEqual( { ...task, done: 1 } );
			( await loadSerialized( Secret, secret.uuid ) ).should.deepEqual( secret );
		} );

		it( "removes items of non-privately exposed models for non-privileged users while rejecting guests", async() => {
			let [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( guest, john ) => {
				( await query( guest, "todo:delete", todo.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:delete", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid } } );
				todo = await createTodo( ctx );
				( await query( guest, "todo:remove", todo.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "todo:remove", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid } } );

				( await query( guest, "task:delete", task.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:delete", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid } } );
				task = await createTask( ctx );
				( await query( guest, "task:remove", task.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "task:remove", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid } } );

				( await query( guest, "secret:delete", secret.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:delete", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
				( await query( guest, "secret:remove", secret.uuid ) ).should.deepEqual( { error: "access forbidden" } );
				( await query( john, "secret:remove", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			( await Todo.list() ).should.deepEqual( [] );
			( await Task.list() ).should.deepEqual( [] );
			( await Secret.list() ).should.have.length( 1 );
		} );

		it( "removes items of non-privately exposed models for privileged users", async() => {
			let [ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:delete", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid } } );
				( await query( admin, "task:delete", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid } } );

				( await query( admin, "secret:delete", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			const { Todo, Task, Secret } = ctx.hitchy.api.model;

			await new Todo( todo.uuid ).load().should.be.rejected();
			await new Task( task.uuid ).load().should.be.rejected();
			await new Secret( secret.uuid ).load().should.not.be.rejected();

			[ todo, task, secret ] = await Promise.all( [ createTodo( ctx ), createTask( ctx ), createSecret( ctx ) ] );

			await connect( ctx, async( _, ___, admin ) => {
				( await query( admin, "todo:remove", todo.uuid ) ).should.deepEqual( { success: true, properties: { uuid: todo.uuid } } );
				( await query( admin, "task:remove", task.uuid ) ).should.deepEqual( { success: true, properties: { uuid: task.uuid } } );

				( await query( admin, "secret:remove", secret.uuid ) ).should.deepEqual( { error: "access forbidden by model" } );
			} );

			await new Todo( todo.uuid ).load().should.be.rejected();
			await new Task( task.uuid ).load().should.be.rejected();
			await new Secret( secret.uuid ).load().should.not.be.rejected();
		} );
	} );
} );
