"use strict";

const { describe, it, afterEach } = require( "mocha" );
const SDT = require( "@hitchy/server-dev-tools" );
const { io } = require( "socket.io-client" );

require( "should" );

describe( "Websocket server monitoring document-oriented database", () => {
	const ctx = {};

	afterEach( SDT.after( ctx ) );

	const setup = async authz => {
		await SDT.before( ctx, {
			projectFolder: false,
			plugin: true,
			// options: { debug: true },
			files: {
				"api/model/todo.js": `module.exports = { props: { title: {}, secret: { private: true }, prio: { type: "number", protected: true } } }`,
				"api/model/task.js": `module.exports = { props: { label: {}, code: { private: true }, done: { type: "boolean", protected: true } }, options: { expose: "protected" } }`,
				"api/model/secret.js": `module.exports = { props: { name: {}, key: { private: true }, valid: { type: "boolean", protected: true } }, options: { expose: "private" } }`,
				"config/auth.js": `exports.auth = { authorizations: ${JSON.stringify( authz || {} )} }`,
			},
		} )();

		const user = new ctx.hitchy.api.model.User();
		user.name = "john.doe";
		await user.setPassword( "secret" );

		await user.save();

		return {
			url: `http://127.0.0.1:${ctx.server.address().port}/hitchy/odem`,
			user,
		};
	};

	const createTodo = () => {
		const item = new ctx.hitchy.api.models.Todo();

		item.title = "another title";
		item.prio = 123;
		item.secret = "token";

		return item.save();
	};

	const editTodo = async uuid => {
		const item = new ctx.hitchy.api.models.Todo( uuid );
		await item.load();

		item.title = "different title";

		return item.save();
	};

	const removeTodo = uuid => {
		const item = new ctx.hitchy.api.models.Todo( uuid );

		return item.remove();
	};

	const createTask = () => {
		const item = new ctx.hitchy.api.models.Task();

		item.label = "task label";
		item.code = "123-456";
		item.done = false;

		return item.save();
	};

	const editTask = async uuid => {
		const item = new ctx.hitchy.api.models.Task( uuid );
		await item.load();

		item.label = "updated task label";

		return item.save();
	};

	const removeTask = uuid => {
		const item = new ctx.hitchy.api.models.Task( uuid );

		return item.remove();
	};

	const createSecret = () => {
		const item = new ctx.hitchy.api.models.Secret();

		item.name = "THE secret";
		item.key = "123-456";
		item.valid = true;

		return item.save();
	};

	const editSecret = async uuid => {
		const item = new ctx.hitchy.api.models.Secret( uuid );
		await item.load();

		item.name = "the SECRET";

		return item.save();
	};

	const removeSecret = uuid => {
		const item = new ctx.hitchy.api.models.Secret( uuid );

		return item.remove();
	};

	const authenticatedSocket = async( url, name = "admin", password = "nimda" ) => {
		const res = await ctx.post( "/api/auth/login", `username=${name}&password=${password}`, {
			"content-type": "application/x-www-form-urlencoded",
		} );

		res.statusCode.should.equal( 200 );

		let sid = null;

		( res.headers["set-cookie"] || [] ).some( cookie => {
			const match = /^sessionId=\s*([^;]+)/.exec( cookie );

			if ( match ) {
				sid = match[1];
				return true;
			}

			return false;
		} );

		return io( url, {
			extraHeaders: {
				cookie: `sessionId=${sid}`,
			}
		} );
	};

	const awaitMessage = ( url, event, action, socket = undefined ) => {
		return new Promise( ( resolve, reject ) => {
			const _socket = socket || io( url );
			const timer = setTimeout( reject, 500, new Error( "did not get a notification in time" ) );

			_socket.once( "connect_error", reject );
			_socket.once( event, result => {
				_socket.close();
				clearTimeout( timer );

				resolve( result );
			} );

			_socket.once( "connect", action );
		} );
	};

	const awaitNoMessage = ( url, event, action, socket = undefined ) => {
		return new Promise( ( resolve, reject ) => {
			const _socket = socket || io( url );

			_socket.once( "connect_error", reject );
			_socket.once( event, () => {
				_socket.close();
				reject( new Error( "got notified unexpectedly" ) );
			} );

			_socket.once( "connect", action );

			setTimeout( resolve, 200 );
		} );
	};

	const countTrigger = ( count, fn ) => {
		return () => {
			if ( !--count ) {
				fn();
			}
		};
	};

	const openAccess = {
		"@hitchy.odem": "*",
	};

	const publicExposeAccess = {
		"@hitchy.odem.model.Todo.expose": "*",
		"@hitchy.odem.model.Task.expose": "*",
		"@hitchy.odem.model.Secret.expose": "*",
	};

	const regularReadAccess = {
		...publicExposeAccess,
		"@hitchy.odem.model.Todo.read": "john.doe",
		"@hitchy.odem.model.Task.read": "john.doe",
		"@hitchy.odem.model.Secret.read": "john.doe",
	};

	const regularReadPropertiesAccess = {
		...regularReadAccess,
		"@hitchy.odem.model.Todo.property.prio.read": "+john.doe",
		"@hitchy.odem.model.Task.property.done.read": "+john.doe",
		"@hitchy.odem.model.Secret.property.valid.read": "+john.doe",
	};

	const getThreeListeners = async( url, event, action, awaiting = true ) => {
		const trigger = countTrigger( 3, action );

		return await Promise.all( [
			( awaiting ? awaitMessage : awaitNoMessage )( url, event, trigger ),
			( awaiting ? awaitMessage : awaitNoMessage )( url, event, trigger, await authenticatedSocket( url, "john.doe", "secret" ) ),
			( awaiting ? awaitMessage : awaitNoMessage )( url, event, trigger, await authenticatedSocket( url ) ),
		] );
	};

	describe( "by default", () => {
		it( "emits a model-specific broadcast on creation of a public model's item including properties visible to either listening user", async() => {
			let item;
			const { url } = await setup();
			const [ guest, john, admin ] = await getThreeListeners( url, "todo:changed", async() => {
				item = await createTodo();
			} );

			guest.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {}
			} );

			admin.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: item.title,
					prio: item.prio,
				}
			} );
		} );

		it( "emits a model-specific broadcast on update of a public model's item including properties visible to either listening user", async() => {
			const { url } = await setup();
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "todo:changed", () => editTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {}
			} );

			admin.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: "different title",
					prio: item.prio,
				}
			} );
		} );

		it( "emits a model-specific broadcast on removal of a public model's item hiding removed item's UUID from non-privileged users", async() => {
			const { url } = await setup();
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "todo:changed", () => removeTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {}
			} );

			admin.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {
					uuid: item.uuid
				}
			} );
		} );

		it( "does not emit a model-specific broadcast on creation of a protectedly exposed model's item", async() => {
			const { url } = await setup();
			await getThreeListeners( url, "task:changed", async() => await createTask(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a model-specific broadcast on update of a protectedly exposed model's item", async() => {
			const { url } = await setup();
			const item = await createTask();
			await getThreeListeners( url, "task:changed", () => editTask( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a model-specific broadcast on removal of a protectedly exposed model's item", async() => {
			const { url } = await setup();
			const item = await createTask();
			await getThreeListeners( url, "task:changed", () => removeTask( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a model-specific broadcast on creation of a privately exposed model's item", async() => {
			const { url } = await setup();
			await getThreeListeners( url, "secret:changed", async() => await createSecret(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a model-specific broadcast on update of a privately exposed model's item", async() => {
			const { url } = await setup();
			const item = await createSecret();
			await getThreeListeners( url, "secret:changed", () => editSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a model-specific broadcast on removal of a privately exposed model's item", async() => {
			const { url } = await setup();
			const item = await createSecret();
			await getThreeListeners( url, "secret:changed", () => removeSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );


		it( "does not emit a broadcast specific to different model on creation of a public model's item", async() => {
			const { url } = await setup();
			await getThreeListeners( url, "task:changed", async() => await createTodo(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on update of a public model's item", async() => {
			const { url } = await setup();
			const item = await createTodo();
			await getThreeListeners( url, "task:changed", () => editTodo( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on removal of a public model's item", async() => {
			const { url } = await setup();
			const item = await createTodo();
			await getThreeListeners( url, "task:changed", () => removeTodo( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on creation of a protectedly exposed model's item", async() => {
			const { url } = await setup();
			await getThreeListeners( url, "todo:changed", async() => await createTask(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on update of a protectedly exposed model's item", async() => {
			const { url } = await setup();
			const item = await createTask();
			await getThreeListeners( url, "todo:changed", () => editTask( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on removal of a protectedly exposed model's item", async() => {
			const { url } = await setup();
			const item = await createTask();
			await getThreeListeners( url, "todo:changed", () => removeTask( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on creation of a non-exposed model's item", async() => {
			const { url } = await setup();
			await getThreeListeners( url, "todo:changed", async() => await createSecret(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on update of a non-exposed model's item", async() => {
			const { url } = await setup();
			const item = await createSecret();
			await getThreeListeners( url, "todo:changed", () => editSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on removal of a non-exposed model's item", async() => {
			const { url } = await setup();
			const item = await createSecret();
			await getThreeListeners( url, "todo:changed", () => removeSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );


		it( "emits a generic broadcast on creation of a public model's item including properties visible to either listening user", async() => {
			let item;
			const { url } = await setup();
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", async() => {
				item = await createTodo();
			} );

			guest.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {}
			} );

			admin.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: item.title,
					prio: item.prio,
				}
			} );
		} );

		it( "emits a generic broadcast on update of a public model's item including properties visible to either listening user", async() => {
			const { url } = await setup();
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", () => editTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {}
			} );

			admin.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: "different title",
					prio: item.prio,
				}
			} );
		} );

		it( "emits a generic broadcast on removal of a public model's item hiding removed item's UUID from non-privileged users", async() => {
			const { url } = await setup();
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", () => removeTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {}
			} );

			admin.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {
					uuid: item.uuid,
				}
			} );
		} );

		it( "does not emit a generic broadcast on creation of a protectedly exposed model's item", async() => {
			const { url } = await setup();
			await getThreeListeners( url, "*:changed", async() => await createTask(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a generic broadcast on update of a protectedly exposed model's item", async() => {
			const { url } = await setup();
			const item = await createTask();
			await getThreeListeners( url, "*:changed", () => editTask( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a generic broadcast on removal of a protectedly exposed model's item", async() => {
			const { url } = await setup();
			const item = await createTask();
			await getThreeListeners( url, "*:changed", () => removeTask( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a generic broadcast on creation of a privately exposed model's item", async() => {
			const { url } = await setup();
			await getThreeListeners( url, "*:changed", async() => await createSecret(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a generic broadcast on update of a privately exposed model's item", async() => {
			const { url } = await setup();
			const item = await createSecret();
			await getThreeListeners( url, "*:changed", () => editSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a generic broadcast on removal of a privately exposed model's item", async() => {
			const { url } = await setup();
			const item = await createSecret();
			await getThreeListeners( url, "*:changed", () => removeSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );
	} );

	describe( "with access on everything explicitly granted to everyone", () => {
		it( "emits a model-specific broadcast on creation of a public model's item including all non-private properties", async() => {
			const { url } = await setup( openAccess );
			let item;
			const [ guest, john, admin ] = await getThreeListeners( url, "todo:changed", async() => {
				item = await createTodo();
			} );

			guest.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: item.title,
					prio: item.prio,
				}
			} );

			john.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: item.title,
					prio: item.prio,
				}
			} );

			admin.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: item.title,
					prio: item.prio,
				}
			} );
		} );

		it( "emits a model-specific broadcast on update of a public model's item including all non-private properties", async() => {
			const { url } = await setup( openAccess );
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "todo:changed", () => editTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: "different title",
					prio: item.prio,
				}
			} );

			john.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: "different title",
					prio: item.prio,
				}
			} );

			admin.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: "different title",
					prio: item.prio,
				}
			} );
		} );

		it( "emits a model-specific broadcast on removal of a public model's item including removed item's UUID", async() => {
			const { url } = await setup( openAccess );
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "todo:changed", () => removeTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {
					uuid: item.uuid
				}
			} );

			john.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {
					uuid: item.uuid
				}
			} );

			admin.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {
					uuid: item.uuid
				}
			} );
		} );

		it( "emits a generic broadcast on creation of a public model's item including all non-private properties", async() => {
			const { url } = await setup( openAccess );
			let item;
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", async() => {
				item = await createTodo();
			} );

			guest.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: item.title,
					prio: item.prio,
				}
			} );

			john.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: item.title,
					prio: item.prio,
				}
			} );

			admin.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: item.title,
					prio: item.prio,
				}
			} );
		} );

		it( "emits a generic broadcast on update of a public model's item including all non-private properties", async() => {
			const { url } = await setup( openAccess );
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", () => editTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: "different title",
					prio: item.prio,
				}
			} );

			john.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: "different title",
					prio: item.prio,
				}
			} );

			admin.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: "different title",
					prio: item.prio,
				}
			} );
		} );

		it( "emits a generic broadcast on removal of a public model's item including removed item's UUID", async() => {
			const { url } = await setup( openAccess );
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", () => removeTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {
					uuid: item.uuid
				}
			} );

			john.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {
					uuid: item.uuid
				}
			} );

			admin.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {
					uuid: item.uuid
				}
			} );
		} );


		it( "emits a model-specific broadcast on creation of a protectedly exposed model's item including all non-private properties", async() => {
			const { url } = await setup( openAccess );
			let item;
			const [ guest, john, admin ] = await getThreeListeners( url, "task:changed", async() => {
				item = await createTask();
			} );

			guest.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "task label",
					done: 0,
				}
			} );

			john.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "task label",
					done: 0,
				}
			} );

			admin.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "task label",
					done: 0,
				}
			} );
		} );

		it( "emits a model-specific broadcast on update of a protectedly exposed model's item including all non-private properties", async() => {
			const { url } = await setup( openAccess );
			const item = await createTask();
			const [ guest, john, admin ] = await getThreeListeners( url, "task:changed", () => editTask( item.uuid ) );

			guest.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "updated task label",
					done: 0
				}
			} );

			john.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "updated task label",
					done: 0
				}
			} );

			admin.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "updated task label",
					done: 0
				}
			} );
		} );

		it( "emits a model-specific broadcast on removal of a protectedly exposed model's item including removed item's UUID", async() => {
			const { url } = await setup( openAccess );
			const item = await createTask();
			const [ guest, john, admin ] = await getThreeListeners( url, "task:changed", () => removeTask( item.uuid ) );

			guest.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {
					uuid: item.uuid,
				}
			} );

			john.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {
					uuid: item.uuid,
				}
			} );

			admin.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {
					uuid: item.uuid,
				}
			} );
		} );

		it( "emits a generic broadcast on creation of a protectedly exposed model's item including all non-private properties", async() => {
			const { url } = await setup( openAccess );
			let item;
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", async() => {
				item = await createTask();
			} );

			guest.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "task label",
					done: 0,
				}
			} );

			john.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "task label",
					done: 0,
				}
			} );

			admin.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "task label",
					done: 0,
				}
			} );
		} );

		it( "emits a generic broadcast on update of a protectedly exposed model's item including all non-private properties", async() => {
			const { url } = await setup( openAccess );
			const item = await createTask();
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", () => editTask( item.uuid ) );

			guest.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "updated task label",
					done: 0
				}
			} );

			john.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "updated task label",
					done: 0
				}
			} );

			admin.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "updated task label",
					done: 0
				}
			} );
		} );

		it( "emits a generic broadcast on removal of a protectedly exposed model's item including removed item's UUID", async() => {
			const { url } = await setup( openAccess );
			const item = await createTask();
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", () => removeTask( item.uuid ) );

			guest.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {
					uuid: item.uuid,
				}
			} );

			john.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {
					uuid: item.uuid,
				}
			} );

			admin.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {
					uuid: item.uuid,
				}
			} );
		} );


		it( "does not emit a model-specific broadcast to guests on creation of a privately exposed model's item", async() => {
			const { url } = await setup( openAccess );
			await getThreeListeners( url, "secret:changed", async() => await createSecret(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a model-specific broadcast to guests on update of a privately exposed model's item", async() => {
			const { url } = await setup( openAccess );
			const item = await createSecret();
			await getThreeListeners( url, "secret:changed", () => editSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a model-specific broadcast to guests on removal of a privately exposed model's item", async() => {
			const { url } = await setup( openAccess );
			const item = await createSecret();
			await getThreeListeners( url, "secret:changed", () => removeSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a generic broadcast to guests on creation of a privately exposed model's item", async() => {
			const { url } = await setup( openAccess );
			await getThreeListeners( url, "*:changed", async() => await createSecret(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a generic broadcast on update of a privately exposed model's item", async() => {
			const { url } = await setup( openAccess );
			const item = await createSecret();
			await getThreeListeners( url, "*:changed", () => editSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a generic broadcast on removal of a privately exposed model's item", async() => {
			const { url } = await setup( openAccess );
			const item = await createSecret();
			await getThreeListeners( url, "*:changed", () => removeSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );
	} );

	describe( "with access exposing protected models to the public", () => {
		it( "emits a model-specific broadcast on creation of a public model's item including properties visible to either listening user", async() => {
			let item;
			const { url } = await setup( publicExposeAccess );
			const [ guest, john, admin ] = await getThreeListeners( url, "todo:changed", async() => {
				item = await createTodo();
			} );

			guest.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {}
			} );

			admin.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: item.title,
					prio: item.prio,
				}
			} );
		} );

		it( "emits a model-specific broadcast on update of a public model's item including properties visible to either listening user", async() => {
			const { url } = await setup( publicExposeAccess );
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "todo:changed", () => editTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {}
			} );

			admin.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: "different title",
					prio: item.prio,
				}
			} );
		} );

		it( "emits a model-specific broadcast on removal of a public model's item hiding removed item's UUID from non-privileged users", async() => {
			const { url } = await setup( publicExposeAccess );
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "todo:changed", () => removeTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {}
			} );

			admin.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {
					uuid: item.uuid
				}
			} );
		} );

		it( "emits a model-specific broadcast on creation of a protectedly exposed model's item including properties visible to either listening user", async() => {
			let item;
			const { url } = await setup( publicExposeAccess );
			const [ guest, john, admin ] = await getThreeListeners( url, "task:changed", async() => {
				item = await createTask();
			} );

			guest.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {}
			} );

			admin.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: item.label,
					done: 0,
				}
			} );
		} );

		it( "emits a model-specific broadcast on update of a protectedly exposed model's item including properties visible to either listening user", async() => {
			const { url } = await setup( publicExposeAccess );
			const item = await createTask();
			const [ guest, john, admin ] = await getThreeListeners( url, "task:changed", () => editTask( item.uuid ) );

			guest.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {}
			} );

			admin.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "updated task label",
					done: 0,
				}
			} );
		} );

		it( "emits a model-specific broadcast on removal of a protectedly exposed model's item including removed item's UUID for privileged users", async() => {
			const { url } = await setup( publicExposeAccess );
			const item = await createTask();
			const [ guest, john, admin ] = await getThreeListeners( url, "task:changed", () => removeTask( item.uuid ) );

			guest.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {}
			} );

			admin.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {
					uuid: item.uuid
				}
			} );
		} );

		it( "does not emit a model-specific broadcast on creation of a privately exposed model's item", async() => {
			const { url } = await setup( publicExposeAccess );
			await getThreeListeners( url, "secret:changed", async() => await createSecret(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a model-specific broadcast on update of a privately exposed model's item", async() => {
			const { url } = await setup( publicExposeAccess );
			const item = await createSecret();
			await getThreeListeners( url, "secret:changed", () => editSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a model-specific broadcast on removal of a privately exposed model's item", async() => {
			const { url } = await setup( publicExposeAccess );
			const item = await createSecret();
			await getThreeListeners( url, "secret:changed", () => removeSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );


		it( "does not emit a broadcast specific to different model on creation of a public model's item", async() => {
			const { url } = await setup( publicExposeAccess );
			await getThreeListeners( url, "task:changed", async() => await createTodo(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on update of a public model's item", async() => {
			const { url } = await setup( publicExposeAccess );
			const item = await createTodo();
			await getThreeListeners( url, "task:changed", () => editTodo( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on removal of a public model's item", async() => {
			const { url } = await setup( publicExposeAccess );
			const item = await createTodo();
			await getThreeListeners( url, "task:changed", () => removeTodo( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on creation of a protectedly exposed model's item", async() => {
			const { url } = await setup( publicExposeAccess );
			await getThreeListeners( url, "todo:changed", async() => await createTask(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on update of a protectedly exposed model's item", async() => {
			const { url } = await setup( publicExposeAccess );
			const item = await createTask();
			await getThreeListeners( url, "todo:changed", () => editTask( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on removal of a protectedly exposed model's item", async() => {
			const { url } = await setup( publicExposeAccess );
			const item = await createTask();
			await getThreeListeners( url, "todo:changed", () => removeTask( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on creation of a non-exposed model's item", async() => {
			const { url } = await setup( publicExposeAccess );
			await getThreeListeners( url, "todo:changed", async() => await createSecret(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on update of a non-exposed model's item", async() => {
			const { url } = await setup( publicExposeAccess );
			const item = await createSecret();
			await getThreeListeners( url, "todo:changed", () => editSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on removal of a non-exposed model's item", async() => {
			const { url } = await setup( publicExposeAccess );
			const item = await createSecret();
			await getThreeListeners( url, "todo:changed", () => removeSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );


		it( "emits a generic broadcast on creation of a public model's item including properties visible to either listening user", async() => {
			let item;
			const { url } = await setup( publicExposeAccess );
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", async() => {
				item = await createTodo();
			} );

			guest.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {}
			} );

			admin.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: item.title,
					prio: item.prio,
				}
			} );
		} );

		it( "emits a generic broadcast on update of a public model's item including properties visible to either listening user", async() => {
			const { url } = await setup( publicExposeAccess );
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", () => editTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {}
			} );

			admin.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: "different title",
					prio: item.prio,
				}
			} );
		} );

		it( "emits a generic broadcast on removal of a public model's item hiding removed item's UUID from non-privileged users", async() => {
			const { url } = await setup( publicExposeAccess );
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", () => removeTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {}
			} );

			admin.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {
					uuid: item.uuid,
				}
			} );
		} );

		it( "emits a generic broadcast on creation of a protectedly exposed model's item including properties visible to either listening user", async() => {
			let item;
			const { url } = await setup( publicExposeAccess );
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", async() => {
				item = await createTask();
			} );

			guest.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {}
			} );

			admin.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: item.label,
					done: 0,
				}
			} );
		} );

		it( "emits a generic broadcast on update of a protectedly exposed model's item including properties visible to either listening user", async() => {
			const { url } = await setup( publicExposeAccess );
			const item = await createTask();
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", () => editTask( item.uuid ) );

			guest.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {}
			} );

			admin.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "updated task label",
					done: 0,
				}
			} );
		} );

		it( "emits a generic broadcast on removal of a protectedly exposed model's item including removed item's UUID for privileged users", async() => {
			const { url } = await setup( publicExposeAccess );
			const item = await createTask();
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", () => removeTask( item.uuid ) );

			guest.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {}
			} );

			admin.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {
					uuid: item.uuid
				}
			} );
		} );

		it( "does not emit a generic broadcast on creation of a privately exposed model's item", async() => {
			const { url } = await setup( publicExposeAccess );
			await getThreeListeners( url, "*:changed", async() => await createSecret(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a generic broadcast on update of a privately exposed model's item", async() => {
			const { url } = await setup( publicExposeAccess );
			const item = await createSecret();
			await getThreeListeners( url, "*:changed", () => editSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a generic broadcast on removal of a privately exposed model's item", async() => {
			const { url } = await setup( publicExposeAccess );
			const item = await createSecret();
			await getThreeListeners( url, "*:changed", () => removeSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );
	} );

	describe( "with access exposing protected models to the public and basic read access to models to particular user", () => {
		it( "emits a model-specific broadcast on creation of a public model's item including properties visible to either listening user", async() => {
			let item;
			const { url } = await setup( regularReadAccess );
			const [ guest, john, admin ] = await getThreeListeners( url, "todo:changed", async() => {
				item = await createTodo();
			} );

			guest.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: item.title,
				}
			} );

			admin.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: item.title,
					prio: item.prio,
				}
			} );
		} );

		it( "emits a model-specific broadcast on update of a public model's item including properties visible to either listening user", async() => {
			const { url } = await setup( regularReadAccess );
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "todo:changed", () => editTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: "different title",
				}
			} );

			admin.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: "different title",
					prio: item.prio,
				}
			} );
		} );

		it( "emits a model-specific broadcast on removal of a public model's item hiding removed item's UUID from unauthorized users", async() => {
			const { url } = await setup( regularReadAccess );
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "todo:changed", () => removeTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {
					uuid: item.uuid
				}
			} );

			admin.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {
					uuid: item.uuid
				}
			} );
		} );

		it( "emits a model-specific broadcast on creation of a protectedly exposed model's item including properties visible to either listening user", async() => {
			let item;
			const { url } = await setup( regularReadAccess );
			const [ guest, john, admin ] = await getThreeListeners( url, "task:changed", async() => {
				item = await createTask();
			} );

			guest.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: item.label,
				}
			} );

			admin.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: item.label,
					done: 0,
				}
			} );
		} );

		it( "emits a model-specific broadcast on update of a protectedly exposed model's item including properties visible to either listening user", async() => {
			const { url } = await setup( regularReadAccess );
			const item = await createTask();
			const [ guest, john, admin ] = await getThreeListeners( url, "task:changed", () => editTask( item.uuid ) );

			guest.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "updated task label",
				}
			} );

			admin.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "updated task label",
					done: 0,
				}
			} );
		} );

		it( "emits a model-specific broadcast on removal of a protectedly exposed model's item including removed item's UUID for authorized users", async() => {
			const { url } = await setup( regularReadAccess );
			const item = await createTask();
			const [ guest, john, admin ] = await getThreeListeners( url, "task:changed", () => removeTask( item.uuid ) );

			guest.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {
					uuid: item.uuid
				}
			} );

			admin.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {
					uuid: item.uuid
				}
			} );
		} );

		it( "does not emit a model-specific broadcast on creation of a privately exposed model's item", async() => {
			const { url } = await setup( regularReadAccess );
			await getThreeListeners( url, "secret:changed", async() => await createSecret(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a model-specific broadcast on update of a privately exposed model's item", async() => {
			const { url } = await setup( regularReadAccess );
			const item = await createSecret();
			await getThreeListeners( url, "secret:changed", () => editSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a model-specific broadcast on removal of a privately exposed model's item", async() => {
			const { url } = await setup( regularReadAccess );
			const item = await createSecret();
			await getThreeListeners( url, "secret:changed", () => removeSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );


		it( "does not emit a broadcast specific to different model on creation of a public model's item", async() => {
			const { url } = await setup( regularReadAccess );
			await getThreeListeners( url, "task:changed", async() => await createTodo(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on update of a public model's item", async() => {
			const { url } = await setup( regularReadAccess );
			const item = await createTodo();
			await getThreeListeners( url, "task:changed", () => editTodo( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on removal of a public model's item", async() => {
			const { url } = await setup( regularReadAccess );
			const item = await createTodo();
			await getThreeListeners( url, "task:changed", () => removeTodo( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on creation of a protectedly exposed model's item", async() => {
			const { url } = await setup( regularReadAccess );
			await getThreeListeners( url, "todo:changed", async() => await createTask(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on update of a protectedly exposed model's item", async() => {
			const { url } = await setup( regularReadAccess );
			const item = await createTask();
			await getThreeListeners( url, "todo:changed", () => editTask( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on removal of a protectedly exposed model's item", async() => {
			const { url } = await setup( regularReadAccess );
			const item = await createTask();
			await getThreeListeners( url, "todo:changed", () => removeTask( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on creation of a non-exposed model's item", async() => {
			const { url } = await setup( regularReadAccess );
			await getThreeListeners( url, "todo:changed", async() => await createSecret(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on update of a non-exposed model's item", async() => {
			const { url } = await setup( regularReadAccess );
			const item = await createSecret();
			await getThreeListeners( url, "todo:changed", () => editSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on removal of a non-exposed model's item", async() => {
			const { url } = await setup( regularReadAccess );
			const item = await createSecret();
			await getThreeListeners( url, "todo:changed", () => removeSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );


		it( "emits a generic broadcast on creation of a public model's item including properties visible to either listening user", async() => {
			let item;
			const { url } = await setup( regularReadAccess );
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", async() => {
				item = await createTodo();
			} );

			guest.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: item.title,
				}
			} );

			admin.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: item.title,
					prio: item.prio,
				}
			} );
		} );

		it( "emits a generic broadcast on update of a public model's item including properties visible to either listening user", async() => {
			const { url } = await setup( regularReadAccess );
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", () => editTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: "different title",
				}
			} );

			admin.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: "different title",
					prio: item.prio,
				}
			} );
		} );

		it( "emits a generic broadcast on removal of a public model's item hiding removed item's UUID from unauthorized users", async() => {
			const { url } = await setup( regularReadAccess );
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", () => removeTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {
					uuid: item.uuid,
				}
			} );

			admin.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {
					uuid: item.uuid,
				}
			} );
		} );

		it( "emits a generic broadcast on creation of a protectedly exposed model's item including properties visible to either listening user", async() => {
			let item;
			const { url } = await setup( regularReadAccess );
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", async() => {
				item = await createTask();
			} );

			guest.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: item.label,
				}
			} );

			admin.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: item.label,
					done: 0,
				}
			} );
		} );

		it( "emits a generic broadcast on update of a protectedly exposed model's item including properties visible to either listening user", async() => {
			const { url } = await setup( regularReadAccess );
			const item = await createTask();
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", () => editTask( item.uuid ) );

			guest.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "updated task label",
				}
			} );

			admin.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "updated task label",
					done: 0,
				}
			} );
		} );

		it( "emits a generic broadcast on removal of a protectedly exposed model's item including removed item's UUID for authorized users", async() => {
			const { url } = await setup( regularReadAccess );
			const item = await createTask();
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", () => removeTask( item.uuid ) );

			guest.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {
					uuid: item.uuid
				}
			} );

			admin.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {
					uuid: item.uuid
				}
			} );
		} );

		it( "does not emit a generic broadcast on creation of a privately exposed model's item", async() => {
			const { url } = await setup( regularReadAccess );
			await getThreeListeners( url, "*:changed", async() => await createSecret(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a generic broadcast on update of a privately exposed model's item", async() => {
			const { url } = await setup( regularReadAccess );
			const item = await createSecret();
			await getThreeListeners( url, "*:changed", () => editSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a generic broadcast on removal of a privately exposed model's item", async() => {
			const { url } = await setup( regularReadAccess );
			const item = await createSecret();
			await getThreeListeners( url, "*:changed", () => removeSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );
	} );

	describe( "with access exposing protected models to the public and basic read access to models and their protected properties to particular user", () => {
		it( "emits a model-specific broadcast on creation of a public model's item including properties visible to either listening user", async() => {
			let item;
			const { url } = await setup( regularReadPropertiesAccess );
			const [ guest, john, admin ] = await getThreeListeners( url, "todo:changed", async() => {
				item = await createTodo();
			} );

			guest.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: item.title,
					prio: item.prio,
				}
			} );

			admin.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: item.title,
					prio: item.prio,
				}
			} );
		} );

		it( "emits a model-specific broadcast on update of a public model's item including properties visible to either listening user", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "todo:changed", () => editTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: "different title",
					prio: item.prio,
				}
			} );

			admin.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: "different title",
					prio: item.prio,
				}
			} );
		} );

		it( "emits a model-specific broadcast on removal of a public model's item hiding removed item's UUID from unauthorized users", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "todo:changed", () => removeTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {
					uuid: item.uuid
				}
			} );

			admin.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {
					uuid: item.uuid
				}
			} );
		} );

		it( "emits a model-specific broadcast on creation of a protectedly exposed model's item including properties visible to either listening user", async() => {
			let item;
			const { url } = await setup( regularReadPropertiesAccess );
			const [ guest, john, admin ] = await getThreeListeners( url, "task:changed", async() => {
				item = await createTask();
			} );

			guest.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: item.label,
					done: 0,
				}
			} );

			admin.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: item.label,
					done: 0,
				}
			} );
		} );

		it( "emits a model-specific broadcast on update of a protectedly exposed model's item including properties visible to either listening user", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			const item = await createTask();
			const [ guest, john, admin ] = await getThreeListeners( url, "task:changed", () => editTask( item.uuid ) );

			guest.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "updated task label",
					done: 0,
				}
			} );

			admin.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "updated task label",
					done: 0,
				}
			} );
		} );

		it( "emits a model-specific broadcast on removal of a protectedly exposed model's item including removed item's UUID for authorized users", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			const item = await createTask();
			const [ guest, john, admin ] = await getThreeListeners( url, "task:changed", () => removeTask( item.uuid ) );

			guest.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {
					uuid: item.uuid
				}
			} );

			admin.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {
					uuid: item.uuid
				}
			} );
		} );

		it( "does not emit a model-specific broadcast on creation of a privately exposed model's item", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			await getThreeListeners( url, "secret:changed", async() => await createSecret(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a model-specific broadcast on update of a privately exposed model's item", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			const item = await createSecret();
			await getThreeListeners( url, "secret:changed", () => editSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a model-specific broadcast on removal of a privately exposed model's item", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			const item = await createSecret();
			await getThreeListeners( url, "secret:changed", () => removeSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );


		it( "does not emit a broadcast specific to different model on creation of a public model's item", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			await getThreeListeners( url, "task:changed", async() => await createTodo(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on update of a public model's item", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			const item = await createTodo();
			await getThreeListeners( url, "task:changed", () => editTodo( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on removal of a public model's item", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			const item = await createTodo();
			await getThreeListeners( url, "task:changed", () => removeTodo( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on creation of a protectedly exposed model's item", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			await getThreeListeners( url, "todo:changed", async() => await createTask(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on update of a protectedly exposed model's item", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			const item = await createTask();
			await getThreeListeners( url, "todo:changed", () => editTask( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on removal of a protectedly exposed model's item", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			const item = await createTask();
			await getThreeListeners( url, "todo:changed", () => removeTask( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on creation of a non-exposed model's item", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			await getThreeListeners( url, "todo:changed", async() => await createSecret(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on update of a non-exposed model's item", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			const item = await createSecret();
			await getThreeListeners( url, "todo:changed", () => editSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a broadcast specific to different model on removal of a non-exposed model's item", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			const item = await createSecret();
			await getThreeListeners( url, "todo:changed", () => removeSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );


		it( "emits a generic broadcast on creation of a public model's item including properties visible to either listening user", async() => {
			let item;
			const { url } = await setup( regularReadPropertiesAccess );
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", async() => {
				item = await createTodo();
			} );

			guest.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: item.title,
					prio: item.prio,
				}
			} );

			admin.should.deepEqual( {
				change: "created",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: item.title,
					prio: item.prio,
				}
			} );
		} );

		it( "emits a generic broadcast on update of a public model's item including properties visible to either listening user", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", () => editTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: "different title",
					prio: item.prio,
				}
			} );

			admin.should.deepEqual( {
				change: "updated",
				model: "Todo",
				properties: {
					uuid: item.uuid,
					title: "different title",
					prio: item.prio,
				}
			} );
		} );

		it( "emits a generic broadcast on removal of a public model's item hiding removed item's UUID from unauthorized users", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			const item = await createTodo();
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", () => removeTodo( item.uuid ) );

			guest.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {
					uuid: item.uuid,
				}
			} );

			admin.should.deepEqual( {
				change: "deleted",
				model: "Todo",
				properties: {
					uuid: item.uuid,
				}
			} );
		} );

		it( "emits a generic broadcast on creation of a protectedly exposed model's item including properties visible to either listening user", async() => {
			let item;
			const { url } = await setup( regularReadPropertiesAccess );
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", async() => {
				item = await createTask();
			} );

			guest.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: item.label,
					done: 0,
				}
			} );

			admin.should.deepEqual( {
				change: "created",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: item.label,
					done: 0,
				}
			} );
		} );

		it( "emits a generic broadcast on update of a protectedly exposed model's item including properties visible to either listening user", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			const item = await createTask();
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", () => editTask( item.uuid ) );

			guest.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "updated task label",
					done: 0,
				}
			} );

			admin.should.deepEqual( {
				change: "updated",
				model: "Task",
				properties: {
					uuid: item.uuid,
					label: "updated task label",
					done: 0,
				}
			} );
		} );

		it( "emits a generic broadcast on removal of a protectedly exposed model's item including removed item's UUID for authorized users", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			const item = await createTask();
			const [ guest, john, admin ] = await getThreeListeners( url, "*:changed", () => removeTask( item.uuid ) );

			guest.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {}
			} );

			john.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {
					uuid: item.uuid
				}
			} );

			admin.should.deepEqual( {
				change: "deleted",
				model: "Task",
				properties: {
					uuid: item.uuid
				}
			} );
		} );

		it( "does not emit a generic broadcast on creation of a privately exposed model's item", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			await getThreeListeners( url, "*:changed", async() => await createSecret(), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a generic broadcast on update of a privately exposed model's item", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			const item = await createSecret();
			await getThreeListeners( url, "*:changed", () => editSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );

		it( "does not emit a generic broadcast on removal of a privately exposed model's item", async() => {
			const { url } = await setup( regularReadPropertiesAccess );
			const item = await createSecret();
			await getThreeListeners( url, "*:changed", () => removeSecret( item.uuid ), false )
				.should.be.resolvedWith( [ undefined, undefined, undefined ] );
		} );
	} );
} );
