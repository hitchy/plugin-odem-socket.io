"use strict";

module.exports = function() {
	const api = this;

	return {
		initialize() {
			api.services.OdemWebsocketProvider.start();
		},
	};
};
