"use strict";

module.exports = {
	socket: {
		odem: {
			/**
			 * Controls if server-side socket is handling client-side requests
			 * for creating, reading, updating and deleting (CRUD) model items.
			 *
			 * Currently, this is false by default due to the lack of obeying
			 * authorization control.
			 */
			crud: false,

			/**
			 * Controls if server-side socket is broadcasting events on
			 * notifications emitted by either server-side model on change.
			 */
			notifications: true,
		},
	},
};
